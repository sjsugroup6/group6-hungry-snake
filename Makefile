.PHONY: clean build run

CC=gcc
SNAKE_DIR=projects/lpc40xx_freertos/snake
SRC_DIR=$(SNAKE_DIR)/sources
CFLAGS=-I$(SNAKE_DIR) -g3 -fstack-protector
BUILD_DIR=_build_snake

_DEPS = game.h map.h scene.h menu.h sprite.h collision.h
DEPS = $(patsubst %,$(SNAKE_DIR)/%,$(_DEPS))

_OBJ = game.o map.o scene.o menu.o sprite.o collision.o
OBJ = $(patsubst %,$(SRC_DIR)/%,$(_OBJ))

MAIN_OBJ = main.o

BIN=$(BUILD_DIR)/snake

build: $(OBJ) $(MAIN_OBJ)
	mkdir -p $(BUILD_DIR)
	$(CC) -o $(BIN) $^ $(CFLAGS)

clean:
	rm -rf $(BUILD_DIR) $(MAIN_OBJ)
	find $(SNAKE_DIR) -name "*.o" -exec rm {} \;

run: clean build
	$(BIN)
