#include <stdio.h>
#include <stdlib.h>

#include "map.h"

int main(void) {
  game_grid_t *gg = init_easy_map(NULL, NULL, NULL, NULL, NULL);
  // Assume the snake is the first element
  sprite_t *snake = gg->sprite_list->sprite;

  print_game_state(gg);
  for (uint32_t i = 0; i < 3; i++) {
    if (game_step(gg)) {
      break;
    }
    print_game_state(gg);
  }

  print_game_state(gg);
  return 0;
}
