#ifndef __LED_MATRIX__
#define __LED_MATRIX__

#include "delay.h"
#include "gpio_lab.h"
#include "lpc40xx.h"
#include "pwm1.h"
#include <stdio.h>
#include <string.h>

// LED matrix driver for SJSU CMPE244 FALL 2020 GROUP 6
// Game: Hungry Snake

#define LED_MATRIX_HALF_LENGTH 32
#define LED_MATRIX_FULL_LENGTH 64

#define BLACK                                                                                                          \
  (rgb_color_type) { 0, 0, 0 }
#define RED                                                                                                            \
  (rgb_color_type) { 1, 0, 0 }
#define GREEN                                                                                                          \
  (rgb_color_type) { 0, 1, 0 }
#define YELLOW                                                                                                         \
  (rgb_color_type) { 1, 1, 0 }
#define BLUE                                                                                                           \
  (rgb_color_type) { 0, 0, 1 }
#define PINK                                                                                                           \
  (rgb_color_type) { 1, 0, 1 }
#define CYAN                                                                                                           \
  (rgb_color_type) { 0, 1, 1 }
#define WHITE                                                                                                          \
  (rgb_color_type) { 1, 1, 1 }

/*typedef enum {
  BLACK = 0,
  RED = 1,
  GREEN = 2,
  YELLOW = 3,
  BLUE = 4,
  PINK = 5,
  CYAN = 6,
  WHITE = 7,

} color_t;*/

// This struct saves the value of R G and B in either 1 or 0 (GPIO)
typedef struct {
  uint8_t R;
  uint8_t G;
  uint8_t B; // GPIO ONLY
} __attribute__((__packed__)) rgb_color_type;

void led_matrix__init_pins(void);
void led_matrix__refreshDisplay(void);
void led_matrix__row_scan(uint8_t row);
void led_matrix__clock_in_data(uint8_t row);
void led_matrix__select_row(uint8_t row);
void led_matrix__disable_output(void);
void led_matrix__enable_output(void);
void led_matrix__latch_data(void);
void led_matrix__unlatch_data(void);
void led_matrix__printMatrix(int row);

// pixel-related functions
void led_matrix__drawPixel(uint8_t row, uint8_t col, rgb_color_type color);
void led_matrix__clearPixel(uint8_t row, uint8_t col);
void led__matrix_clear_screen(void);

#endif