#include "music.h"

// port: 0
static const uint32_t music_pin_1 = 15; // connects to P2_2 on the music board
static const uint32_t music_pin_2 = 17; // connects to P2_4 on the music board
static const uint32_t music_pin_3 = 18; // connects to P2_5 on the music board
static const uint32_t music_pin_4 = 22; // connects to P2_6 on the music board

/*
Song list:
0 0 0 0 -> N/A
1 0 0 0 -> bgm
0 1 0 0 -> dead
0 0 1 0 -> eat
0 0 0 1 -> faster
*/

void init_music_pins(void) {
  // output direciton, gpio function
  LPC_GPIO0->DIR |= 1 << music_pin_1 | 1 << music_pin_2 | 1 << music_pin_3 | 1 << music_pin_4;
  LPC_IOCON->P0_15 &= ~(0x7);
  LPC_IOCON->P0_16 &= ~(0x7);
  LPC_IOCON->P0_17 &= ~(0x7);
  LPC_IOCON->P0_18 &= ~(0x7);
  LPC_GPIO0->PIN &= ~(1 << music_pin_1 | 1 << music_pin_2 | 1 << music_pin_3 | 1 << music_pin_4);

  fprintf(stderr, "Pins initialized\n");
}

void play_bgm_music(void) {
  // the idea is to only set the pins once
  /*uint32_t music_mask = 0xFFFFFFFF & (~(1 << music_pin_1 | 1 << music_pin_2 | 1 << music_pin_3 | 1 << music_pin_4));
  uint32_t original = LPC_GPIO0->PIN;
  LPC_GPIO0->PIN = original & (music_mask | 1 << music_pin_1);
  delay__ms(10);
  LPC_GPIO0->PIN = original & music_mask;*/

  LPC_GPIO0->PIN &= ~(1 << music_pin_1 | 1 << music_pin_2 | 1 << music_pin_3 | 1 << music_pin_4);
  LPC_GPIO0->PIN |= (1 << music_pin_1);
  fprintf(stderr, "Before delay BGM pin value: %lu\n", LPC_GPIO0->PIN & (1 << (music_pin_1)));
  delay__ms(175);
  fprintf(stderr, "After delay BGM pin value: %lu\n", LPC_GPIO0->PIN & (1 << (music_pin_1)));
  LPC_GPIO0->PIN &= ~(1 << music_pin_1 | 1 << music_pin_2 | 1 << music_pin_3 | 1 << music_pin_4);
  fprintf(stderr, "After reset BGM pin values: %lu\n",
          LPC_GPIO0->PIN & (1 << music_pin_1 | 1 << music_pin_2 | 1 << music_pin_3 | 1 << music_pin_4));
}

void play_faster_music(void) {
  // the idea is to only set the pins once
  /*uint32_t music_mask = 0xFFFFFFFF & (~(1 << music_pin_1 | 1 << music_pin_2 | 1 << music_pin_3 | 1 << music_pin_4));
  uint32_t original = LPC_GPIO0->PIN;
  LPC_GPIO0->PIN = original & (music_mask | 1 << music_pin_4);
  delay__ms(10);
  LPC_GPIO0->PIN = original & music_mask;*/

  LPC_GPIO0->PIN &= ~(1 << music_pin_1 | 1 << music_pin_2 | 1 << music_pin_3 | 1 << music_pin_4);
  LPC_GPIO0->PIN |= (1 << music_pin_4);
  fprintf(stderr, "Before delay FASTER pin value: %lu\n", LPC_GPIO0->PIN & (1 << (music_pin_4)));
  delay__ms(175);
  fprintf(stderr, "After delay FASTER pin value: %lu\n", LPC_GPIO0->PIN & (1 << (music_pin_4)));
  LPC_GPIO0->PIN &= ~(1 << music_pin_1 | 1 << music_pin_2 | 1 << music_pin_3 | 1 << music_pin_4);
  fprintf(stderr, "After reset FASTER pin values: %lu\n",
          LPC_GPIO0->PIN & (1 << music_pin_1 | 1 << music_pin_2 | 1 << music_pin_3 | 1 << music_pin_4));
}

void play_dead_sound(void) {
  // the idea is to only set the pins once
  /*uint32_t music_mask = 0xFFFFFFFF & (~(1 << music_pin_1 | 1 << music_pin_2 | 1 << music_pin_3 | 1 << music_pin_4));
  uint32_t original = LPC_GPIO0->PIN;
  LPC_GPIO0->PIN = original & (music_mask | 1 << music_pin_3);
  delay__ms(10);
  LPC_GPIO0->PIN = original & music_mask;*/

  LPC_GPIO0->PIN &= ~(1 << music_pin_1 | 1 << music_pin_2 | 1 << music_pin_3 | 1 << music_pin_4);
  LPC_GPIO0->PIN |= (1 << music_pin_2);
  fprintf(stderr, "Before delay DEAD pin value: %lu\n", LPC_GPIO0->PIN & (1 << (music_pin_2)));
  delay__ms(175);
  fprintf(stderr, "After delay DEAD pin value: %lu\n", LPC_GPIO0->PIN & (1 << (music_pin_2)));
  LPC_GPIO0->PIN &= ~(1 << music_pin_1 | 1 << music_pin_2 | 1 << music_pin_3 | 1 << music_pin_4);
  fprintf(stderr, "After reset DEAD pin values: %lu\n",
          LPC_GPIO0->PIN & (1 << music_pin_1 | 1 << music_pin_2 | 1 << music_pin_3 | 1 << music_pin_4));
}

void play_eat_sound(void) {
  /*uint32_t music_mask = 0xFFFFFFFF & (~(1 << music_pin_1 | 1 << music_pin_2 | 1 << music_pin_3 | 1 << music_pin_4));
  uint32_t original = LPC_GPIO0->PIN;
  LPC_GPIO0->PIN = original & (music_mask | 1 << music_pin_2);
  delay__ms(10);
  LPC_GPIO0->PIN = original & music_mask;*/

  LPC_GPIO0->PIN &= ~(1 << music_pin_1 | 1 << music_pin_2 | 1 << music_pin_3 | 1 << music_pin_4);
  LPC_GPIO0->PIN |= (1 << music_pin_3);
  fprintf(stderr, "Before delay EAT pin value: %lu\n", LPC_GPIO0->PIN & (1 << (music_pin_3)));
  delay__ms(175);
  fprintf(stderr, "After delay EAT pin value: %lu\n", LPC_GPIO0->PIN & (1 << (music_pin_3)));
  LPC_GPIO0->PIN &= ~(1 << music_pin_1 | 1 << music_pin_2 | 1 << music_pin_3 | 1 << music_pin_4);
  fprintf(stderr, "After reset EAT pin values: %lu\n",
          LPC_GPIO0->PIN & (1 << music_pin_1 | 1 << music_pin_2 | 1 << music_pin_3 | 1 << music_pin_4));
}
