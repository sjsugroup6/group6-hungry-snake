#include "LED_matrix.h"

// uncomment this to have R1, R2, G1, G2 as PWM, comment this for the pins to be GPIO
//#define PWM

// LED matrix driver for SJSU CMPE244 FALL 2020 GROUP 6
// Game: Hungry Snake

// Pins for the LED matrix
static port_pin_type LAT = {1, 30};
static port_pin_type CLK = {1, 29};
static port_pin_type OE = {0, 16};
static port_pin_type R1 = {2, 0};
static port_pin_type G1 = {2, 1};
static port_pin_type B1 = {2, 2};
static port_pin_type R2 = {2, 4};
static port_pin_type G2 = {2, 5};
static port_pin_type B2 = {2, 6};
static port_pin_type A = {2, 7};
static port_pin_type B = {2, 8};
static port_pin_type C = {2, 9};
static port_pin_type D = {1, 20};
static port_pin_type E = {1, 23};

// for each pixel, 24 bits for RGB (R=8, G=8, B=8)
static rgb_color_type display_matrix_top[LED_MATRIX_HALF_LENGTH][LED_MATRIX_FULL_LENGTH] = {0};
static rgb_color_type display_matrix_bottom[LED_MATRIX_HALF_LENGTH][LED_MATRIX_FULL_LENGTH] = {0};

#ifdef PWM
static uint32_t MR0_value;
#endif

void led_matrix__init_pins(void) {

  // ALL gpio function except R1, G1, R2, G2. Those are PWM function (function 1)
  const uint8_t function_1_bit = 1;

  LPC_IOCON->P2_0 &= ~0x7;
  LPC_IOCON->P2_1 &= ~0x7;
  LPC_IOCON->P2_4 &= ~0x7;
  LPC_IOCON->P2_5 &= ~0x7;

#ifdef PWM
  LPC_IOCON->P2_0 |= function_1_bit;
  LPC_IOCON->P2_1 |= function_1_bit;
  LPC_IOCON->P2_4 |= function_1_bit;
  LPC_IOCON->P2_5 |= function_1_bit;
#endif

  lab_gpio__set_as_output(LAT.port, LAT.pin);
  lab_gpio__set_as_output(CLK.port, CLK.pin);
  lab_gpio__set_as_output(OE.port, OE.pin);
  lab_gpio__set_as_output(A.port, A.pin);
  lab_gpio__set_as_output(B.port, B.pin);
  lab_gpio__set_as_output(C.port, C.pin);
  lab_gpio__set_as_output(D.port, D.pin);
  lab_gpio__set_as_output(E.port, E.pin);
  lab_gpio__set_as_output(B1.port, B1.pin);
  lab_gpio__set_as_output(B2.port, B2.pin);

  lab_gpio__set_low(LAT.port, LAT.pin);
  lab_gpio__set_low(CLK.port, CLK.pin);
  lab_gpio__set_low(OE.port, OE.pin);
  lab_gpio__set_low(A.port, A.pin);
  lab_gpio__set_low(B.port, B.pin);
  lab_gpio__set_low(C.port, C.pin);
  lab_gpio__set_low(D.port, D.pin);
  lab_gpio__set_low(E.port, E.pin);
  lab_gpio__set_low(B1.port, B1.pin);
  lab_gpio__set_low(B2.port, B2.pin);

#ifdef PWM
  pwm1__init_single_edge(9600000); // slower hz doesn't work, the higher the better
  MR0_value = LPC_PWM1->MR0;
#else
  lab_gpio__set_as_output(R1.port, R1.pin);
  lab_gpio__set_as_output(G1.port, G1.pin);
  lab_gpio__set_as_output(R2.port, R2.pin);
  lab_gpio__set_as_output(G2.port, G2.pin);
#endif

  // memset(display_matrix_top, 0, LED_MATRIX_HALF_LENGTH * LED_MATRIX_FULL_LENGTH);    // TESTING
  // memset(display_matrix_bottom, 0, LED_MATRIX_HALF_LENGTH * LED_MATRIX_FULL_LENGTH); // TESTING
}

void led_matrix__refreshDisplay(void) {
  for (uint8_t row = 0; row < LED_MATRIX_HALF_LENGTH; row++) {

    led_matrix__select_row(row);
    led_matrix__disable_output(); // gpio__set(OE);
    led_matrix__unlatch_data();   // gpio__set(LAT);
    led_matrix__clock_in_data(row);
    led_matrix__latch_data();     // gpio__reset(LAT);
    led_matrix__enable_output();  // gpio__reset(OE);
    delay__us(100);               // Change Brightness
    led_matrix__disable_output(); // gpio__set(OE);
  }
}

void led_matrix__row_scan(uint8_t row) {
  led_matrix__select_row(row);
  led_matrix__disable_output(); // gpio__set(OE);
  led_matrix__unlatch_data();   // gpio__set(LAT);
  led_matrix__clock_in_data(row);
  led_matrix__latch_data();     // gpio__reset(LAT);
  led_matrix__enable_output();  // gpio__reset(OE);
  delay__us(100);               // Change Brightness
  led_matrix__disable_output(); // gpio__set(OE);
}

void led_matrix__clock_in_data(uint8_t row) {
// Clock in data for each column
#ifdef PWM
  uint32_t match_reg_value;
#endif
  for (uint8_t col = 0; col < LED_MATRIX_FULL_LENGTH; col++) {

// R1 = PWM1__2_0, G1 = PWM1__2_1, R2 = PWM1__2_4, G2 = PWM1__2_5

/*match_reg_value = (MR0_value * display_matrix_top[row][col].R) / 100;
LPC_PWM1->MR1 = match_reg_value;

match_reg_value = (MR0_value * display_matrix_top[row][col].G) / 100;
LPC_PWM1->MR2 = match_reg_value;

match_reg_value = (MR0_value * display_matrix_bottom[row][col].R) / 100;
LPC_PWM1->MR5 = match_reg_value;

match_reg_value = (MR0_value * display_matrix_bottom[row][col].G) / 100;
LPC_PWM1->MR6 = match_reg_value;*/
#ifdef PWM
    LPC_GPIO2->PIN &= ~(1 << B1.pin | 1 << B2.pin);
    LPC_PWM1->MR1 = display_matrix_top[row][col].R;
    LPC_PWM1->MR2 = display_matrix_top[row][col].G;
    LPC_PWM1->MR5 = display_matrix_bottom[row][col].R;
    LPC_PWM1->MR6 = display_matrix_bottom[row][col].G;

    LPC_PWM1->LER |= (1 << 1 | 1 << 2 | 1 << 5 | 1 << 6); ///< Enable Latch Register
    // B1 B2
    LPC_GPIO2->PIN |= (display_matrix_top[row][col].B << B1.pin) | (display_matrix_bottom[row][col].B << B2.pin);

#else
    LPC_GPIO2->PIN &= ~((1 << R1.pin) | (1 << R2.pin) | (1 << G1.pin) | (1 << G2.pin) | (1 << B1.pin) | (1 << B2.pin));

    LPC_GPIO2->PIN |= (display_matrix_top[row][col].R << R1.pin) | (display_matrix_bottom[row][col].R << R2.pin) |
                      (display_matrix_top[row][col].G << G1.pin) | (display_matrix_bottom[row][col].G << G2.pin) |
                      (display_matrix_top[row][col].B << B1.pin) | (display_matrix_bottom[row][col].B << B2.pin);
#endif
    /*pwm1__set_duty_cycle(PWM1__2_4, display_matrix_bottom[row][col].R);
    pwm1__set_duty_cycle(PWM1__2_5, display_matrix_bottom[row][col].G);

    pwm1__set_duty_cycle(PWM1__2_0, display_matrix_top[row][col].R);
    pwm1__set_duty_cycle(PWM1__2_1, display_matrix_top[row][col].G);*/

    lab_gpio__set(CLK.port, CLK.pin, true);
    lab_gpio__set(CLK.port, CLK.pin, false);
  }
}

void led_matrix__select_row(uint8_t row) {
  // set all rows to low first
  LPC_GPIO1->PIN &= ~(1 << D.pin | 1 << E.pin);
  LPC_GPIO2->PIN &= ~(1 << A.pin | 1 << B.pin | 1 << C.pin);

  // row number selected using A to E (5 bits)
  LPC_GPIO2->PIN |= (((row >> 0) & 0x1) << A.pin | ((row >> 1) & 0x1) << B.pin | ((row >> 2) & 0x1) << C.pin);
  LPC_GPIO1->PIN |= (((row >> 3) & 0x1) << D.pin | ((row >> 4) & 0x1) << E.pin);
}

void led_matrix__disable_output(void) { lab_gpio__set(OE.port, OE.pin, true); }

void led_matrix__enable_output(void) { lab_gpio__set(OE.port, OE.pin, false); }

void led_matrix__unlatch_data(void) { lab_gpio__set(LAT.port, LAT.pin, true); }

void led_matrix__latch_data(void) { lab_gpio__set(LAT.port, LAT.pin, false); }

void led_matrix__printMatrix(int row) {
  if (row < LED_MATRIX_HALF_LENGTH) {
    for (int col = 0; col < LED_MATRIX_FULL_LENGTH; col++) {
      fprintf(stderr, "row: %d, col: %d, R: %d, G: %d, B: %d \n", row, col, display_matrix_top[row][col].R,
              display_matrix_top[row][col].G, display_matrix_top[row][col].B);
    }
  } else {
    uint8_t lower_row = row % LED_MATRIX_HALF_LENGTH;
    for (int col = 0; col < LED_MATRIX_FULL_LENGTH; col++) {
      fprintf(stderr, "row: %d, col: %d, R: %d, G: %d, B: %d \n", row, col, display_matrix_bottom[lower_row][col].R,
              display_matrix_bottom[lower_row][col].G, display_matrix_bottom[lower_row][col].B);
    }
  }
}

// pixel-related functions
void led_matrix__drawPixel(uint8_t row, uint8_t col, rgb_color_type color) {

  if (row >= LED_MATRIX_FULL_LENGTH)
    return;
  if (col >= LED_MATRIX_FULL_LENGTH)
    return;
  if (row >= LED_MATRIX_HALF_LENGTH) // bottom row pixel
  {
    display_matrix_bottom[row % LED_MATRIX_HALF_LENGTH][col] = color;
  } else { // top row pixel
    display_matrix_top[row][col] = color;
  }
}

void led_matrix__clearPixel(uint8_t row, uint8_t col) {

  if (row >= LED_MATRIX_FULL_LENGTH)
    return;
  if (col >= LED_MATRIX_FULL_LENGTH)
    return;

  if (row >= LED_MATRIX_HALF_LENGTH) // bottom row pixel
  {
    display_matrix_bottom[row % LED_MATRIX_HALF_LENGTH][col] = (rgb_color_type){0};
  } else { // top row pixel
    display_matrix_top[row][col] = (rgb_color_type){0};
    ;
  }
}

void led__matrix_clear_screen(void) {
  memset(display_matrix_top, 0, LED_MATRIX_HALF_LENGTH * LED_MATRIX_FULL_LENGTH * sizeof(rgb_color_type));
  memset(display_matrix_bottom, 0, LED_MATRIX_HALF_LENGTH * LED_MATRIX_FULL_LENGTH * sizeof(rgb_color_type));
}