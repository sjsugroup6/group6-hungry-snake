#include <stdint.h>

#include "adc.h"
#include "clock.h"
#include "lpc40xx.h"
#include "lpc_peripherals.h"
#include "stdio.h"

/*******************************************************************************
 *
 *                      P U B L I C    F U N C T I O N S
 *
 ******************************************************************************/

void adc__initialize(void) {
  lpc_peripheral__turn_on_power_to(LPC_PERIPHERAL__ADC);

  // Set PDN bit - ADC is operational
  const uint32_t enable_adc_mask = (1 << 21);
  LPC_ADC->CR = enable_adc_mask;

  const uint32_t max_adc_clock = (12 * 1000UL * 1000UL); // 12.4Mhz : max ADC clock in datasheet for lpc40xx
  const uint32_t adc_clock = clock__get_peripheral_clock_hz();

  // APB clock divider to support max ADC clock
  // ADC clock might be different, but this method finds the smallest divider
  // that yields th maximum adc clock
  for (uint32_t divider = 2; divider < 255; divider += 2) {
    if ((adc_clock / divider) < max_adc_clock) {
      LPC_ADC->CR |= (divider << 8);
      break;
    }
  }
}

uint16_t adc__get_adc_value(adc_channel_e channel_num) {
  uint16_t result = 0;
  const uint16_t twelve_bits = 0x0FFF;
  const uint32_t channel_masks = 0xFF;              // bit 0:7 ->SEL bits
  const uint32_t start_conversion = (1 << 24);      // bit 24-26 are start bits. 0x1 = start conversion now
  const uint32_t start_conversion_mask = (7 << 24); // 3bits - B26:B25:B24
  const uint32_t adc_conversion_complete = (1 << 31);

  if ((ADC__CHANNEL_2 == channel_num) || (ADC__CHANNEL_4 == channel_num) || (ADC__CHANNEL_5 == channel_num)) {
    LPC_ADC->CR &= ~(channel_masks | start_conversion_mask); // clear both SEL and START bits
    // Set the channel number and start the conversion now
    LPC_ADC->CR |= (1 << channel_num) | start_conversion;

    while (!(LPC_ADC->GDR & adc_conversion_complete)) { // Wait till conversion is complete (DONE bit)
      ;
    }
    result = (LPC_ADC->GDR >> 4) & twelve_bits; // 12bits - B15:B4 (RESULT bits)
  }

  return result;
}
void adc__enable_burst_mode(adc_channel_e channel_number) {
  if ((ADC__CHANNEL_2 == channel_number) || (ADC__CHANNEL_4 == channel_number) || (ADC__CHANNEL_5 == channel_number)) {
    // Set the channel number and start the conversion now
    LPC_ADC->CR &= ~(0xFF);               // Clear SEL bits
    LPC_ADC->CR |= (1 << channel_number); // choose channel

    uint8_t START_bits = 0x7 << 24;
    LPC_ADC->CR &= ~(START_bits); // Set START bits to 0x0
    LPC_ADC->CR |= (1 << 16);     // set BURST bit
  }
}

void adc__enable_more_channel_burst_mode(adc_channel_e channel_number) {
  if ((ADC__CHANNEL_2 == channel_number) || (ADC__CHANNEL_4 == channel_number) || (ADC__CHANNEL_5 == channel_number)) {
    // Set the channel number and start the conversion now
    LPC_ADC->CR |= (1 << channel_number); // choose channel

    uint8_t START_bits = 0x7 << 24;
    LPC_ADC->CR &= ~(START_bits); // Set START bits to 0x0
    LPC_ADC->CR |= (1 << 16);     // set BURST bit
  }
}

uint16_t adc__get_channel_reading_with_burst_mode(adc_channel_e channel_number) {
  uint16_t result = 0;
  if ((ADC__CHANNEL_2 == channel_number) || (ADC__CHANNEL_4 == channel_number) || (ADC__CHANNEL_5 == channel_number)) {
    const uint32_t adc_conversion_complete = (1 << 31);
    const uint16_t twelve_bits = 0x0FFF;
    while (!(LPC_ADC->DR[channel_number] & adc_conversion_complete)) { // Wait till conversion is complete (DONE bit)
      ;
    }
    result = (LPC_ADC->DR[channel_number] >> 4) & twelve_bits; // 12bits - B15:B4 (RESULT bits)
  }
  return result;
}
