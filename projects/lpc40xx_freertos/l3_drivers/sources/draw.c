#include "draw.h"
#include "game.h"

static banner_type current_banner_type = score;

void draw_string(uint8_t row, uint8_t col, char *a_string, int string_size, rgb_color_type color) {
  int width = 0;
  for (int i = 0; i < string_size; i++) {
    char letter = a_string[i];
    width = draw_letter(letter, row, col, color);
    col = col + width;
    // clear the column right after the letter
    led_matrix__clearPixel(row, col);
    led_matrix__clearPixel(row + 1, col);
    led_matrix__clearPixel(row + 2, col);
    led_matrix__clearPixel(row + 3, col);
    led_matrix__clearPixel(row + 4, col);
    col += 1;
  }
}

void draw_sprites(sprite_list_t *sprite_list) {
  sprite_list_t *list = sprite_list;
  for (; list != NULL; list = list->next) {
    sprite_t *current_sprite = list->sprite;
    sprite_type_t sprite_type = current_sprite->type;
    switch (sprite_type) {
    case SNAKE:
      draw_snake(current_sprite);
      break;

    case ENEMY:
      draw_enemy(current_sprite);
      break;

    case FRUIT:
      draw_fruit(current_sprite);
      break;

    case OBSTACLE:
      draw_obstacle(current_sprite);
      break;

    default:
      fprintf(stderr, "invalid sprite type, nothing to draw \n");
    }
  }
}
void draw_snake(sprite_t *snake) {
  // snake color: green and yellow alternating color
  // walk through the linked list
  body_t *body = snake->head;
  rgb_color_type current_color = GREEN;
  for (; body != NULL; body = body->prev) {
    led_matrix__drawPixel(body->x, body->y, current_color);
    if (current_color.R == GREEN.R && current_color.G == GREEN.G && current_color.B == GREEN.B) {
      current_color = YELLOW;
    } else {
      current_color = GREEN;
    }
  }
}

void draw_enemy(sprite_t *enemy) {
  // enemy color: red and yellow alternating color
  // walk through the linked list
  body_t *body = enemy->head;
  rgb_color_type current_color = RED;
  for (; body != NULL; body = body->prev) {
    led_matrix__drawPixel(body->x, body->y, current_color);
    if (current_color.R == YELLOW.R && current_color.G == YELLOW.G && current_color.B == YELLOW.B) {
      current_color = RED;
    } else {
      current_color = YELLOW;
    }
  }
}

void draw_obstacle(sprite_t *obstacle) {
  // obstacle color: BLUE
  // walk through the linked list
  body_t *body = obstacle->head;
  for (; body != NULL; body = body->prev) {
    led_matrix__drawPixel(body->x, body->y, BLUE);
  }
}

void draw_fruit(sprite_t *fruit) { led_matrix__drawPixel(fruit->head->x, fruit->head->y, RED); }

void clear_screen(void) { led__matrix_clear_screen(); }

// maximum digit for a number: 7
void draw_number(uint16_t number, uint8_t x, uint8_t y, rgb_color_type color) {
  char char_arr[7] = {0};
  int current_array_index = 0;

  if (number == 0) {
    draw_letter('0', x, y, color);
  } else {
    while (number) {
      int digit = number % 10;
      char_arr[current_array_index] = digit + 48;
      number /= 10;
      current_array_index++;
    }
    // now, reverse draw each character
    int y_pixel = y;
    for (int i = current_array_index - 1; i >= 0; i--) {
      y_pixel = y_pixel + draw_letter(char_arr[i], x, y_pixel, color);

      // clear the column right after the letter
      led_matrix__clearPixel(x, y_pixel);
      led_matrix__clearPixel(x + 1, y_pixel);
      led_matrix__clearPixel(x + 2, y_pixel);
      led_matrix__clearPixel(x + 3, y_pixel);
      led_matrix__clearPixel(x + 4, y_pixel);

      // increment y for the next letter
      y_pixel += 1;
    }
  }
}

// this function draws numbers and capitalized letters
// row and col are the left top pixel of the letter
// this function returns the width number of the letter
// all letters are height of 5
int draw_letter(char letter, uint8_t row, uint8_t col, rgb_color_type color) {
  int width = 4;
  switch (letter) {
  case '0':
    // row 0
    led_matrix__clearPixel(row, col);
    led_matrix__drawPixel(row, col + 1, color);
    led_matrix__drawPixel(row, col + 2, color);
    led_matrix__clearPixel(row, col + 3);
    // row 1
    led_matrix__drawPixel(row + 1, col, color);
    led_matrix__clearPixel(row + 1, col + 1);
    led_matrix__clearPixel(row + 1, col + 2);
    led_matrix__drawPixel(row + 1, col + 3, color);
    // row 2
    led_matrix__drawPixel(row + 2, col, color);
    led_matrix__drawPixel(row + 2, col + 1, color);
    led_matrix__clearPixel(row + 2, col + 2);
    led_matrix__drawPixel(row + 2, col + 3, color);
    // row 3
    led_matrix__drawPixel(row + 3, col, color);
    led_matrix__clearPixel(row + 3, col + 1);
    led_matrix__drawPixel(row + 3, col + 2, color);
    led_matrix__drawPixel(row + 3, col + 3, color);
    // row 4
    led_matrix__clearPixel(row + 4, col);
    led_matrix__drawPixel(row + 4, col + 1, color);
    led_matrix__drawPixel(row + 4, col + 2, color);
    led_matrix__clearPixel(row + 4, col + 3);
    break;

  case '1':
    // row 0
    led_matrix__clearPixel(row, col);
    led_matrix__clearPixel(row, col + 1);
    led_matrix__drawPixel(row, col + 2, color);
    // row 1
    led_matrix__clearPixel(row + 1, col);
    led_matrix__drawPixel(row + 1, col + 1, color);
    led_matrix__drawPixel(row + 1, col + 2, color);
    // row 2
    led_matrix__drawPixel(row + 2, col, color);
    led_matrix__clearPixel(row + 2, col + 1);
    led_matrix__drawPixel(row + 2, col + 2, color);
    // row 3
    led_matrix__clearPixel(row + 3, col);
    led_matrix__clearPixel(row + 3, col + 1);
    led_matrix__drawPixel(row + 3, col + 2, color);
    // row 4
    led_matrix__clearPixel(row + 4, col);
    led_matrix__clearPixel(row + 4, col + 1);
    led_matrix__drawPixel(row + 4, col + 2, color);

    width = 3;
    break;

  case '2':
    // row 0
    led_matrix__drawPixel(row, col, color);
    led_matrix__drawPixel(row, col + 1, color);
    led_matrix__drawPixel(row, col + 2, color);
    led_matrix__clearPixel(row, col + 3);
    // row 1
    led_matrix__clearPixel(row + 1, col);
    led_matrix__clearPixel(row + 1, col + 1);
    led_matrix__clearPixel(row + 1, col + 2);
    led_matrix__drawPixel(row + 1, col + 3, color);
    // row 2
    led_matrix__clearPixel(row + 2, col);
    led_matrix__clearPixel(row + 2, col + 1);
    led_matrix__drawPixel(row + 2, col + 2, color);
    led_matrix__clearPixel(row + 2, col + 3);
    // row 3
    led_matrix__clearPixel(row + 3, col);
    led_matrix__drawPixel(row + 3, col + 1, color);
    led_matrix__clearPixel(row + 3, col + 2);
    led_matrix__clearPixel(row + 3, col + 3);
    // row 4
    led_matrix__drawPixel(row + 4, col, color);
    led_matrix__drawPixel(row + 4, col + 1, color);
    led_matrix__drawPixel(row + 4, col + 2, color);
    led_matrix__drawPixel(row + 4, col + 3, color);
    break;

  case '3':
    // row 0
    led_matrix__drawPixel(row, col, color);
    led_matrix__drawPixel(row, col + 1, color);
    led_matrix__drawPixel(row, col + 2, color);
    led_matrix__clearPixel(row, col + 3);
    // row 1
    led_matrix__clearPixel(row + 1, col);
    led_matrix__clearPixel(row + 1, col + 1);
    led_matrix__clearPixel(row + 1, col + 2);
    led_matrix__drawPixel(row + 1, col + 3, color);
    // row 2
    led_matrix__clearPixel(row + 2, col);
    led_matrix__drawPixel(row + 2, col + 1, color);
    led_matrix__drawPixel(row + 2, col + 2, color);
    led_matrix__clearPixel(row + 2, col + 3);
    // row 3
    led_matrix__clearPixel(row + 3, col);
    led_matrix__clearPixel(row + 3, col + 1);
    led_matrix__clearPixel(row + 3, col + 2);
    led_matrix__drawPixel(row + 3, col + 3, color);
    // row 4
    led_matrix__drawPixel(row + 4, col, color);
    led_matrix__drawPixel(row + 4, col + 1, color);
    led_matrix__drawPixel(row + 4, col + 2, color);
    led_matrix__clearPixel(row + 4, col + 3);
    break;

  case '4':
    // row 0
    led_matrix__clearPixel(row, col);
    led_matrix__clearPixel(row, col + 1);
    led_matrix__drawPixel(row, col + 2, color);
    led_matrix__drawPixel(row, col + 3, color);
    // row 1
    led_matrix__clearPixel(row + 1, col);
    led_matrix__drawPixel(row + 1, col + 1, color);
    led_matrix__clearPixel(row + 1, col + 2);
    led_matrix__drawPixel(row + 1, col + 3, color);
    // row 2
    led_matrix__drawPixel(row + 2, col, color);
    led_matrix__clearPixel(row + 2, col + 1);
    led_matrix__clearPixel(row + 2, col + 2);
    led_matrix__drawPixel(row + 2, col + 3, color);
    // row 3
    led_matrix__clearPixel(row + 3, col);
    led_matrix__drawPixel(row + 3, col + 1, color);
    led_matrix__drawPixel(row + 3, col + 2, color);
    led_matrix__drawPixel(row + 3, col + 3, color);
    // row 4
    led_matrix__clearPixel(row + 4, col);
    led_matrix__clearPixel(row + 4, col + 1);
    led_matrix__clearPixel(row + 4, col + 2);
    led_matrix__drawPixel(row + 4, col + 3, color);
    break;

  case '5':
    // row 0
    led_matrix__clearPixel(row, col);
    led_matrix__drawPixel(row, col + 1, color);
    led_matrix__drawPixel(row, col + 2, color);
    led_matrix__drawPixel(row, col + 3, color);
    // row 1
    led_matrix__drawPixel(row + 1, col, color);
    led_matrix__clearPixel(row + 1, col + 1);
    led_matrix__clearPixel(row + 1, col + 2);
    led_matrix__clearPixel(row + 1, col + 3);
    // row 2
    led_matrix__drawPixel(row + 2, col, color);
    led_matrix__drawPixel(row + 2, col + 1, color);
    led_matrix__drawPixel(row + 2, col + 2, color);
    led_matrix__clearPixel(row + 2, col + 3);
    // row 3
    led_matrix__clearPixel(row + 3, col);
    led_matrix__clearPixel(row + 3, col + 1);
    led_matrix__clearPixel(row + 3, col + 2);
    led_matrix__drawPixel(row + 3, col + 3, color);
    // row 4
    led_matrix__drawPixel(row + 4, col, color);
    led_matrix__drawPixel(row + 4, col + 1, color);
    led_matrix__drawPixel(row + 4, col + 2, color);
    led_matrix__clearPixel(row + 4, col + 3);
    break;

  case '6':
    // row 0
    led_matrix__clearPixel(row, col);
    led_matrix__drawPixel(row, col + 1, color);
    led_matrix__drawPixel(row, col + 2, color);
    led_matrix__clearPixel(row, col + 3);
    // row 1
    led_matrix__drawPixel(row + 1, col, color);
    led_matrix__clearPixel(row + 1, col + 1);
    led_matrix__clearPixel(row + 1, col + 2);
    led_matrix__clearPixel(row + 1, col + 3);
    // row 2
    led_matrix__drawPixel(row + 2, col, color);
    led_matrix__drawPixel(row + 2, col + 1, color);
    led_matrix__drawPixel(row + 2, col + 2, color);
    led_matrix__clearPixel(row + 2, col + 3);
    // row 3
    led_matrix__drawPixel(row + 3, col, color);
    led_matrix__clearPixel(row + 3, col + 1);
    led_matrix__clearPixel(row + 3, col + 2);
    led_matrix__drawPixel(row + 3, col + 3, color);
    // row 4
    led_matrix__clearPixel(row + 4, col);
    led_matrix__drawPixel(row + 4, col + 1, color);
    led_matrix__drawPixel(row + 4, col + 2, color);
    led_matrix__clearPixel(row + 4, col + 3);
    break;

  case '7':
    // row 0
    led_matrix__drawPixel(row, col, color);
    led_matrix__drawPixel(row, col + 1, color);
    led_matrix__drawPixel(row, col + 2, color);
    led_matrix__drawPixel(row, col + 3, color);
    // row 1
    led_matrix__clearPixel(row + 1, col);
    led_matrix__clearPixel(row + 1, col + 1);
    led_matrix__clearPixel(row + 1, col + 2);
    led_matrix__drawPixel(row + 1, col + 3, color);
    // row 2
    led_matrix__clearPixel(row + 2, col);
    led_matrix__clearPixel(row + 2, col + 1);
    led_matrix__drawPixel(row + 2, col + 2, color);
    led_matrix__clearPixel(row + 2, col + 3);
    // row 3
    led_matrix__clearPixel(row + 3, col);
    led_matrix__drawPixel(row + 3, col + 1, color);
    led_matrix__clearPixel(row + 3, col + 2);
    led_matrix__clearPixel(row + 3, col + 3);
    // row 4
    led_matrix__clearPixel(row + 4, col);
    led_matrix__drawPixel(row + 4, col + 1, color);
    led_matrix__clearPixel(row + 4, col + 2);
    led_matrix__clearPixel(row + 4, col + 3);
    break;

  case '8':
    // row 0
    led_matrix__clearPixel(row, col);
    led_matrix__drawPixel(row, col + 1, color);
    led_matrix__drawPixel(row, col + 2, color);
    led_matrix__clearPixel(row, col + 3);
    // row 1
    led_matrix__drawPixel(row + 1, col, color);
    led_matrix__clearPixel(row + 1, col + 1);
    led_matrix__clearPixel(row + 1, col + 2);
    led_matrix__drawPixel(row + 1, col + 3, color);
    // row 2
    led_matrix__clearPixel(row + 2, col);
    led_matrix__drawPixel(row + 2, col + 1, color);
    led_matrix__drawPixel(row + 2, col + 2, color);
    led_matrix__clearPixel(row + 2, col + 3);
    // row 3
    led_matrix__drawPixel(row + 3, col, color);
    led_matrix__clearPixel(row + 3, col + 1);
    led_matrix__clearPixel(row + 3, col + 2);
    led_matrix__drawPixel(row + 3, col + 3, color);
    ;
    // row 4
    led_matrix__clearPixel(row + 4, col);
    led_matrix__drawPixel(row + 4, col + 1, color);
    led_matrix__drawPixel(row + 4, col + 2, color);
    led_matrix__clearPixel(row + 4, col + 3);
    break;

  case '9':
    // row 0
    led_matrix__clearPixel(row, col);
    led_matrix__drawPixel(row, col + 1, color);
    led_matrix__drawPixel(row, col + 2, color);
    led_matrix__clearPixel(row, col + 3);
    // row 1
    led_matrix__drawPixel(row + 1, col, color);
    led_matrix__clearPixel(row + 1, col + 1);
    led_matrix__clearPixel(row + 1, col + 2);
    led_matrix__drawPixel(row + 1, col + 3, color);
    // row 2
    led_matrix__clearPixel(row + 2, col);
    led_matrix__drawPixel(row + 2, col + 1, color);
    led_matrix__drawPixel(row + 2, col + 2, color);
    led_matrix__drawPixel(row + 2, col + 3, color);
    // row 3
    led_matrix__clearPixel(row + 3, col);
    led_matrix__clearPixel(row + 3, col + 1);
    led_matrix__clearPixel(row + 3, col + 2);
    led_matrix__drawPixel(row + 3, col + 3, color);
    // row 4
    led_matrix__clearPixel(row + 4, col);
    led_matrix__drawPixel(row + 4, col + 1, color);
    led_matrix__drawPixel(row + 4, col + 2, color);
    led_matrix__clearPixel(row + 4, col + 3);
    break;

  case 'A':
    // row 0
    led_matrix__clearPixel(row, col);
    led_matrix__drawPixel(row, col + 1, color);
    led_matrix__drawPixel(row, col + 2, color);
    led_matrix__clearPixel(row, col + 3);
    // row 1
    led_matrix__drawPixel(row + 1, col, color);
    led_matrix__clearPixel(row + 1, col + 1);
    led_matrix__clearPixel(row + 1, col + 2);
    led_matrix__drawPixel(row + 1, col + 3, color);
    // row 2
    led_matrix__drawPixel(row + 2, col, color);
    led_matrix__clearPixel(row + 2, col + 1);
    led_matrix__clearPixel(row + 2, col + 2);
    led_matrix__drawPixel(row + 2, col + 3, color);
    // row 3
    led_matrix__drawPixel(row + 3, col, color);
    led_matrix__drawPixel(row + 3, col + 1, color);
    led_matrix__drawPixel(row + 3, col + 2, color);
    led_matrix__drawPixel(row + 3, col + 3, color);
    // row 4
    led_matrix__drawPixel(row + 4, col, color);
    led_matrix__clearPixel(row + 4, col + 1);
    led_matrix__clearPixel(row + 4, col + 2);
    led_matrix__drawPixel(row + 4, col + 3, color);
    break;

  case 'B':
    // row 0
    led_matrix__drawPixel(row, col, color);
    led_matrix__drawPixel(row, col + 1, color);
    led_matrix__drawPixel(row, col + 2, color);
    led_matrix__clearPixel(row, col + 3);
    // row 1
    led_matrix__drawPixel(row + 1, col, color);
    led_matrix__clearPixel(row + 1, col + 1);
    led_matrix__clearPixel(row + 1, col + 2);
    led_matrix__drawPixel(row + 1, col + 3, color);
    // row 2
    led_matrix__drawPixel(row + 2, col, color);
    led_matrix__drawPixel(row + 2, col + 1, color);
    led_matrix__drawPixel(row + 2, col + 2, color);
    led_matrix__clearPixel(row + 2, col + 3);
    // row 3
    led_matrix__drawPixel(row + 3, col, color);
    led_matrix__clearPixel(row + 3, col + 1);
    led_matrix__clearPixel(row + 3, col + 2);
    led_matrix__drawPixel(row + 3, col + 3, color);
    // row 4
    led_matrix__drawPixel(row + 4, col, color);
    led_matrix__drawPixel(row + 4, col + 1, color);
    led_matrix__drawPixel(row + 4, col + 2, color);
    led_matrix__clearPixel(row + 4, col + 3);
    break;

  case 'C':
    // row 0
    led_matrix__clearPixel(row, col);
    led_matrix__drawPixel(row, col + 1, color);
    led_matrix__drawPixel(row, col + 2, color);
    led_matrix__clearPixel(row, col + 3);
    // row 1
    led_matrix__drawPixel(row + 1, col, color);
    led_matrix__clearPixel(row + 1, col + 1);
    led_matrix__clearPixel(row + 1, col + 2);
    led_matrix__drawPixel(row + 1, col + 3, color);
    // row 2
    led_matrix__drawPixel(row + 2, col, color);
    led_matrix__clearPixel(row + 2, col + 1);
    led_matrix__clearPixel(row + 2, col + 2);
    led_matrix__clearPixel(row + 2, col + 3);
    // row 3
    led_matrix__drawPixel(row + 3, col, color);
    led_matrix__clearPixel(row + 3, col + 1);
    led_matrix__clearPixel(row + 3, col + 2);
    led_matrix__drawPixel(row + 3, col + 3, color);
    // row 4
    led_matrix__clearPixel(row + 4, col);
    led_matrix__drawPixel(row + 4, col + 1, color);
    led_matrix__drawPixel(row + 4, col + 2, color);
    led_matrix__clearPixel(row + 4, col + 3);
    break;

  case 'D':
    // row 0
    led_matrix__drawPixel(row, col, color);
    led_matrix__drawPixel(row, col + 1, color);
    led_matrix__drawPixel(row, col + 2, color);
    led_matrix__clearPixel(row, col + 3);
    // row 1
    led_matrix__drawPixel(row + 1, col, color);
    led_matrix__clearPixel(row + 1, col + 1);
    led_matrix__clearPixel(row + 1, col + 2);
    led_matrix__drawPixel(row + 1, col + 3, color);
    // row 2
    led_matrix__drawPixel(row + 2, col, color);
    led_matrix__clearPixel(row + 2, col + 1);
    led_matrix__clearPixel(row + 2, col + 2);
    led_matrix__drawPixel(row + 2, col + 3, color);
    // row 3
    led_matrix__drawPixel(row + 3, col, color);
    led_matrix__clearPixel(row + 3, col + 1);
    led_matrix__clearPixel(row + 3, col + 2);
    led_matrix__drawPixel(row + 3, col + 3, color);
    // row 4
    led_matrix__drawPixel(row + 4, col, color);
    led_matrix__drawPixel(row + 4, col + 1, color);
    led_matrix__drawPixel(row + 4, col + 2, color);
    led_matrix__clearPixel(row + 4, col + 3);
    break;

  case 'E':
    // row 0
    led_matrix__drawPixel(row, col, color);
    led_matrix__drawPixel(row, col + 1, color);
    led_matrix__drawPixel(row, col + 2, color);
    led_matrix__drawPixel(row, col + 3, color);
    // row 1
    led_matrix__drawPixel(row + 1, col, color);
    led_matrix__clearPixel(row + 1, col + 1);
    led_matrix__clearPixel(row + 1, col + 2);
    led_matrix__clearPixel(row + 1, col + 3);
    // row 2
    led_matrix__drawPixel(row + 2, col, color);
    led_matrix__drawPixel(row + 2, col + 1, color);
    led_matrix__drawPixel(row + 2, col + 2, color);
    led_matrix__clearPixel(row + 2, col + 3);
    // row 3
    led_matrix__drawPixel(row + 3, col, color);
    led_matrix__clearPixel(row + 3, col + 1);
    led_matrix__clearPixel(row + 3, col + 2);
    led_matrix__clearPixel(row + 3, col + 3);
    // row 4
    led_matrix__drawPixel(row + 4, col, color);
    led_matrix__drawPixel(row + 4, col + 1, color);
    led_matrix__drawPixel(row + 4, col + 2, color);
    led_matrix__drawPixel(row + 4, col + 3, color);
    break;

  case 'F':
    // row 0
    led_matrix__drawPixel(row, col, color);
    led_matrix__drawPixel(row, col + 1, color);
    led_matrix__drawPixel(row, col + 2, color);
    led_matrix__drawPixel(row, col + 3, color);
    // row 1
    led_matrix__drawPixel(row + 1, col, color);
    led_matrix__clearPixel(row + 1, col + 1);
    led_matrix__clearPixel(row + 1, col + 2);
    led_matrix__clearPixel(row + 1, col + 3);
    // row 2
    led_matrix__drawPixel(row + 2, col, color);
    led_matrix__drawPixel(row + 2, col + 1, color);
    led_matrix__drawPixel(row + 2, col + 2, color);
    led_matrix__clearPixel(row + 2, col + 3);
    // row 3
    led_matrix__drawPixel(row + 3, col, color);
    led_matrix__clearPixel(row + 3, col + 1);
    led_matrix__clearPixel(row + 3, col + 2);
    led_matrix__clearPixel(row + 3, col + 3);
    // row 4
    led_matrix__drawPixel(row + 4, col, color);
    led_matrix__clearPixel(row + 4, col + 1);
    led_matrix__clearPixel(row + 4, col + 2);
    led_matrix__clearPixel(row + 4, col + 3);
    break;

  case 'G':
    // row 0
    led_matrix__clearPixel(row, col);
    led_matrix__drawPixel(row, col + 1, color);
    led_matrix__drawPixel(row, col + 2, color);
    led_matrix__clearPixel(row, col + 3);
    // row 1
    led_matrix__drawPixel(row + 1, col, color);
    led_matrix__clearPixel(row + 1, col + 1);
    led_matrix__clearPixel(row + 1, col + 2);
    led_matrix__clearPixel(row + 1, col + 3);
    // row 2
    led_matrix__drawPixel(row + 2, col, color);
    led_matrix__clearPixel(row + 2, col + 1);
    led_matrix__drawPixel(row + 2, col + 2, color);
    led_matrix__drawPixel(row + 2, col + 3, color);
    // row 3
    led_matrix__drawPixel(row + 3, col, color);
    led_matrix__clearPixel(row + 3, col + 1);
    led_matrix__clearPixel(row + 3, col + 2);
    led_matrix__drawPixel(row + 3, col + 3, color);
    // row 4
    led_matrix__clearPixel(row + 4, col);
    led_matrix__drawPixel(row + 4, col + 1, color);
    led_matrix__drawPixel(row + 4, col + 2, color);
    led_matrix__clearPixel(row + 4, col + 3);
    break;

  case 'H':
    // row 0
    led_matrix__drawPixel(row, col, color);
    led_matrix__clearPixel(row, col + 1);
    led_matrix__clearPixel(row, col + 2);
    led_matrix__drawPixel(row, col + 3, color);
    // row 1
    led_matrix__drawPixel(row + 1, col, color);
    led_matrix__clearPixel(row + 1, col + 1);
    led_matrix__clearPixel(row + 1, col + 2);
    led_matrix__drawPixel(row + 1, col + 3, color);
    // row 2
    led_matrix__drawPixel(row + 2, col, color);
    led_matrix__drawPixel(row + 2, col + 1, color);
    led_matrix__drawPixel(row + 2, col + 2, color);
    led_matrix__drawPixel(row + 2, col + 3, color);
    // row 3
    led_matrix__drawPixel(row + 3, col, color);
    led_matrix__clearPixel(row + 3, col + 1);
    led_matrix__clearPixel(row + 3, col + 2);
    led_matrix__drawPixel(row + 3, col + 3, color);
    // row 4
    led_matrix__drawPixel(row + 4, col, color);
    led_matrix__clearPixel(row + 4, col + 1);
    led_matrix__clearPixel(row + 4, col + 2);
    led_matrix__drawPixel(row + 4, col + 3, color);
    break;

  case 'I':
    // row 0
    led_matrix__drawPixel(row, col, color);
    led_matrix__drawPixel(row, col + 1, color);
    led_matrix__drawPixel(row, col + 2, color);
    // row 1
    led_matrix__clearPixel(row + 1, col);
    led_matrix__drawPixel(row + 1, col + 1, color);
    led_matrix__clearPixel(row + 1, col + 2);
    // row 2
    led_matrix__clearPixel(row + 2, col);
    led_matrix__drawPixel(row + 2, col + 1, color);
    led_matrix__clearPixel(row + 2, col + 2);
    // row 3
    led_matrix__clearPixel(row + 3, col);
    led_matrix__drawPixel(row + 3, col + 1, color);
    led_matrix__clearPixel(row + 3, col + 2);
    // row 4
    led_matrix__drawPixel(row + 4, col, color);
    led_matrix__drawPixel(row + 4, col + 1, color);
    led_matrix__drawPixel(row + 4, col + 2, color);

    width = 3;
    break;

  case 'J':
    // row 0
    led_matrix__clearPixel(row, col);
    led_matrix__clearPixel(row, col + 1);
    led_matrix__drawPixel(row, col + 2, color);
    led_matrix__drawPixel(row, col + 3, color);
    // row 1
    led_matrix__clearPixel(row + 1, col);
    led_matrix__clearPixel(row + 1, col + 1);
    led_matrix__clearPixel(row + 1, col + 2);
    led_matrix__drawPixel(row + 1, col + 3, color);
    // row 2
    led_matrix__clearPixel(row + 2, col);
    led_matrix__clearPixel(row + 2, col + 1);
    led_matrix__clearPixel(row + 2, col + 2);
    led_matrix__drawPixel(row + 2, col + 3, color);
    // row 3
    led_matrix__drawPixel(row + 3, col, color);
    led_matrix__clearPixel(row + 3, col + 1);
    led_matrix__clearPixel(row + 3, col + 2);
    led_matrix__drawPixel(row + 3, col + 3, color);
    // row 4
    led_matrix__clearPixel(row + 4, col);
    led_matrix__drawPixel(row + 4, col + 1, color);
    led_matrix__drawPixel(row + 4, col + 2, color);
    led_matrix__clearPixel(row + 4, col + 3);
    break;

  case 'K':
    // row 0
    led_matrix__drawPixel(row, col, color);
    led_matrix__clearPixel(row, col + 1);
    led_matrix__clearPixel(row, col + 2);
    led_matrix__drawPixel(row, col + 3, color);
    // row 1
    led_matrix__drawPixel(row + 1, col, color);
    led_matrix__clearPixel(row + 1, col + 1);
    led_matrix__drawPixel(row + 1, col + 2, color);
    led_matrix__clearPixel(row + 1, col + 3);
    // row 2
    led_matrix__drawPixel(row + 2, col, color);
    led_matrix__drawPixel(row + 2, col + 1, color);
    led_matrix__clearPixel(row + 2, col + 2);
    led_matrix__clearPixel(row + 2, col + 3);
    // row 3
    led_matrix__drawPixel(row + 3, col, color);
    led_matrix__clearPixel(row + 3, col + 1);
    led_matrix__drawPixel(row + 3, col + 2, color);
    led_matrix__clearPixel(row + 3, col + 3);
    // row 4
    led_matrix__drawPixel(row + 4, col, color);
    led_matrix__clearPixel(row + 4, col + 1);
    led_matrix__clearPixel(row + 4, col + 2);
    led_matrix__drawPixel(row + 4, col + 3, color);
    break;

  case 'L':
    // row 0
    led_matrix__drawPixel(row, col, color);
    led_matrix__clearPixel(row, col + 1);
    led_matrix__clearPixel(row, col + 2);
    led_matrix__clearPixel(row, col + 3);
    // row 1
    led_matrix__drawPixel(row + 1, col, color);
    led_matrix__clearPixel(row + 1, col + 1);
    led_matrix__clearPixel(row + 1, col + 2);
    led_matrix__clearPixel(row + 1, col + 3);
    // row 2
    led_matrix__drawPixel(row + 2, col, color);
    led_matrix__clearPixel(row + 2, col + 1);
    led_matrix__clearPixel(row + 2, col + 2);
    led_matrix__clearPixel(row + 2, col + 3);
    // row 3
    led_matrix__drawPixel(row + 3, col, color);
    led_matrix__clearPixel(row + 3, col + 1);
    led_matrix__clearPixel(row + 3, col + 2);
    led_matrix__clearPixel(row + 3, col + 3);
    // row 4
    led_matrix__drawPixel(row + 4, col, color);
    led_matrix__drawPixel(row + 4, col + 1, color);
    led_matrix__drawPixel(row + 4, col + 2, color);
    led_matrix__drawPixel(row + 4, col + 3, color);
    break;

  case 'M':
    // row 0
    led_matrix__drawPixel(row, col, color);
    led_matrix__clearPixel(row, col + 1);
    led_matrix__clearPixel(row, col + 2);
    led_matrix__clearPixel(row, col + 3);
    led_matrix__drawPixel(row, col + 4, color);
    // row 1
    led_matrix__drawPixel(row + 1, col, color);
    led_matrix__drawPixel(row + 1, col + 1, color);
    led_matrix__clearPixel(row + 1, col + 2);
    led_matrix__drawPixel(row + 1, col + 3, color);
    led_matrix__drawPixel(row + 1, col + 4, color);
    // row 2
    led_matrix__drawPixel(row + 2, col, color);
    led_matrix__clearPixel(row + 2, col + 1);
    led_matrix__drawPixel(row + 2, col + 2, color);
    led_matrix__clearPixel(row + 2, col + 3);
    led_matrix__drawPixel(row + 2, col + 4, color);
    // row 3
    led_matrix__drawPixel(row + 3, col, color);
    led_matrix__clearPixel(row + 3, col + 1);
    led_matrix__clearPixel(row + 3, col + 2);
    led_matrix__clearPixel(row + 3, col + 3);
    led_matrix__drawPixel(row + 3, col + 4, color);
    // row 4
    led_matrix__drawPixel(row + 4, col, color);
    led_matrix__clearPixel(row + 4, col + 1);
    led_matrix__clearPixel(row + 4, col + 2);
    led_matrix__clearPixel(row + 4, col + 3);
    led_matrix__drawPixel(row + 4, col + 4, color);
    width = 5;
    break;

  case 'N':
    // row 0
    led_matrix__drawPixel(row, col, color);
    led_matrix__clearPixel(row, col + 1);
    led_matrix__clearPixel(row, col + 2);
    led_matrix__drawPixel(row, col + 3, color);
    // row 1
    led_matrix__drawPixel(row + 1, col, color);
    led_matrix__drawPixel(row + 1, col + 1, color);
    led_matrix__clearPixel(row + 1, col + 2);
    led_matrix__drawPixel(row + 1, col + 3, color);
    // row 2
    led_matrix__drawPixel(row + 2, col, color);
    led_matrix__clearPixel(row + 2, col + 1);
    led_matrix__drawPixel(row + 2, col + 2, color);
    led_matrix__drawPixel(row + 2, col + 3, color);
    // row 3
    led_matrix__drawPixel(row + 3, col, color);
    led_matrix__clearPixel(row + 3, col + 1);
    led_matrix__clearPixel(row + 3, col + 2);
    led_matrix__drawPixel(row + 3, col + 3, color);
    // row 4
    led_matrix__drawPixel(row + 4, col, color);
    led_matrix__clearPixel(row + 4, col + 1);
    led_matrix__clearPixel(row + 4, col + 2);
    led_matrix__drawPixel(row + 4, col + 3, color);
    break;

  case 'O':
    // row 0
    led_matrix__clearPixel(row, col);
    led_matrix__drawPixel(row, col + 1, color);
    led_matrix__drawPixel(row, col + 2, color);
    led_matrix__clearPixel(row, col + 3);
    // row 1
    led_matrix__drawPixel(row + 1, col, color);
    led_matrix__clearPixel(row + 1, col + 1);
    led_matrix__clearPixel(row + 1, col + 2);
    led_matrix__drawPixel(row + 1, col + 3, color);
    // row 2
    led_matrix__drawPixel(row + 2, col, color);
    led_matrix__clearPixel(row + 2, col + 1);
    led_matrix__clearPixel(row + 2, col + 2);
    led_matrix__drawPixel(row + 2, col + 3, color);
    // row 3
    led_matrix__drawPixel(row + 3, col, color);
    led_matrix__clearPixel(row + 3, col + 1);
    led_matrix__clearPixel(row + 3, col + 2);
    led_matrix__drawPixel(row + 3, col + 3, color);
    // row 4
    led_matrix__clearPixel(row + 4, col);
    led_matrix__drawPixel(row + 4, col + 1, color);
    led_matrix__drawPixel(row + 4, col + 2, color);
    led_matrix__clearPixel(row + 4, col + 3);
    break;

  case 'P':
    // row 0
    led_matrix__drawPixel(row, col, color);
    led_matrix__drawPixel(row, col + 1, color);
    led_matrix__drawPixel(row, col + 2, color);
    led_matrix__clearPixel(row, col + 3);
    // row 1
    led_matrix__drawPixel(row + 1, col, color);
    led_matrix__clearPixel(row + 1, col + 1);
    led_matrix__clearPixel(row + 1, col + 2);
    led_matrix__drawPixel(row + 1, col + 3, color);
    // row 2
    led_matrix__drawPixel(row + 2, col, color);
    led_matrix__drawPixel(row + 2, col + 1, color);
    led_matrix__drawPixel(row + 2, col + 2, color);
    led_matrix__drawPixel(row + 2, col + 3, color);
    // row 3
    led_matrix__drawPixel(row + 3, col, color);
    led_matrix__clearPixel(row + 3, col + 1);
    led_matrix__clearPixel(row + 3, col + 2);
    led_matrix__clearPixel(row + 3, col + 3);
    // row 4
    led_matrix__drawPixel(row + 4, col, color);
    led_matrix__clearPixel(row + 4, col + 1);
    led_matrix__clearPixel(row + 4, col + 2);
    led_matrix__clearPixel(row + 4, col + 3);
    break;

  case 'Q':
    // row 0
    led_matrix__clearPixel(row, col);
    led_matrix__drawPixel(row, col + 1, color);
    led_matrix__drawPixel(row, col + 2, color);
    led_matrix__clearPixel(row, col + 3);
    led_matrix__clearPixel(row, col + 4);
    // row 1
    led_matrix__drawPixel(row + 1, col, color);
    led_matrix__clearPixel(row + 1, col + 1);
    led_matrix__clearPixel(row + 1, col + 2);
    led_matrix__drawPixel(row + 1, col + 3, color);
    led_matrix__clearPixel(row + 1, col + 4);
    // row 2
    led_matrix__drawPixel(row + 2, col, color);
    led_matrix__clearPixel(row + 2, col + 1);
    led_matrix__clearPixel(row + 2, col + 2);
    led_matrix__drawPixel(row + 2, col + 3, color);
    led_matrix__clearPixel(row + 2, col + 4);
    // row 3
    led_matrix__drawPixel(row + 3, col, color);
    led_matrix__clearPixel(row + 3, col + 1);
    led_matrix__clearPixel(row + 3, col + 2);
    led_matrix__drawPixel(row + 3, col + 3, color);
    led_matrix__clearPixel(row + 3, col + 4);
    // row 4
    led_matrix__clearPixel(row + 4, col);
    led_matrix__drawPixel(row + 4, col + 1, color);
    led_matrix__drawPixel(row + 4, col + 2, color);
    led_matrix__drawPixel(row + 4, col + 3, color);
    led_matrix__drawPixel(row + 4, col + 4, color);

    width = 5;
    break;

  case 'R':
    // row 0
    led_matrix__drawPixel(row, col, color);
    led_matrix__drawPixel(row, col + 1, color);
    led_matrix__drawPixel(row, col + 2, color);
    led_matrix__clearPixel(row, col + 3);
    // row 1
    led_matrix__drawPixel(row + 1, col, color);
    led_matrix__clearPixel(row + 1, col + 1);
    led_matrix__clearPixel(row + 1, col + 2);
    led_matrix__drawPixel(row + 1, col + 3, color);
    // row 2
    led_matrix__drawPixel(row + 2, col, color);
    led_matrix__drawPixel(row + 2, col + 1, color);
    led_matrix__drawPixel(row + 2, col + 2, color);
    led_matrix__drawPixel(row + 2, col + 3, color);
    // row 3
    led_matrix__drawPixel(row + 3, col, color);
    led_matrix__clearPixel(row + 3, col + 1);
    led_matrix__drawPixel(row + 3, col + 2, color);
    led_matrix__clearPixel(row + 3, col + 3);
    // row 4
    led_matrix__drawPixel(row + 4, col, color);
    led_matrix__clearPixel(row + 4, col + 1);
    led_matrix__clearPixel(row + 4, col + 2);
    led_matrix__drawPixel(row + 4, col + 3, color);
    break;

  case 'S':
    // row 0
    led_matrix__clearPixel(row, col);
    led_matrix__drawPixel(row, col + 1, color);
    led_matrix__drawPixel(row, col + 2, color);
    led_matrix__drawPixel(row, col + 3, color);
    // row 1
    led_matrix__drawPixel(row + 1, col, color);
    led_matrix__clearPixel(row + 1, col + 1);
    led_matrix__clearPixel(row + 1, col + 2);
    led_matrix__clearPixel(row + 1, col + 3);
    // row 2
    led_matrix__clearPixel(row + 2, col);
    led_matrix__drawPixel(row + 2, col + 1, color);
    led_matrix__drawPixel(row + 2, col + 2, color);
    led_matrix__clearPixel(row + 2, col + 3);
    // row 3
    led_matrix__clearPixel(row + 3, col);
    led_matrix__clearPixel(row + 3, col + 1);
    led_matrix__clearPixel(row + 3, col + 2);
    led_matrix__drawPixel(row + 3, col + 3, color);
    // row 4
    led_matrix__drawPixel(row + 4, col, color);
    led_matrix__drawPixel(row + 4, col + 1, color);
    led_matrix__drawPixel(row + 4, col + 2, color);
    led_matrix__clearPixel(row + 4, col + 3);
    break;

  case 'T':
    // row 0
    led_matrix__drawPixel(row, col, color);
    led_matrix__drawPixel(row, col + 1, color);
    led_matrix__drawPixel(row, col + 2, color);
    led_matrix__drawPixel(row, col + 3, color);
    led_matrix__drawPixel(row, col + 4, color);
    // row 1
    led_matrix__clearPixel(row + 1, col);
    led_matrix__clearPixel(row + 1, col + 1);
    led_matrix__drawPixel(row + 1, col + 2, color);
    led_matrix__clearPixel(row + 1, col + 3);
    led_matrix__clearPixel(row + 1, col + 4);
    // row 2
    led_matrix__clearPixel(row + 2, col);
    led_matrix__clearPixel(row + 2, col + 1);
    led_matrix__drawPixel(row + 2, col + 2, color);
    led_matrix__clearPixel(row + 2, col + 3);
    led_matrix__clearPixel(row + 2, col + 4);
    // row 3
    led_matrix__clearPixel(row + 3, col);
    led_matrix__clearPixel(row + 3, col + 1);
    led_matrix__drawPixel(row + 3, col + 2, color);
    led_matrix__clearPixel(row + 3, col + 3);
    led_matrix__clearPixel(row + 3, col + 4);
    // row 4
    led_matrix__clearPixel(row + 4, col);
    led_matrix__clearPixel(row + 4, col + 1);
    led_matrix__drawPixel(row + 4, col + 2, color);
    led_matrix__clearPixel(row + 4, col + 3);
    led_matrix__clearPixel(row + 4, col + 4);

    width = 5;
    break;

  case 'U':
    // row 0
    led_matrix__drawPixel(row, col, color);
    led_matrix__clearPixel(row, col + 1);
    led_matrix__clearPixel(row, col + 2);
    led_matrix__drawPixel(row, col + 3, color);
    // row 1
    led_matrix__drawPixel(row + 1, col, color);
    led_matrix__clearPixel(row + 1, col + 1);
    led_matrix__clearPixel(row + 1, col + 2);
    led_matrix__drawPixel(row + 1, col + 3, color);
    // row 2
    led_matrix__drawPixel(row + 2, col, color);
    led_matrix__clearPixel(row + 2, col + 1);
    led_matrix__clearPixel(row + 2, col + 2);
    led_matrix__drawPixel(row + 2, col + 3, color);
    // row 3
    led_matrix__drawPixel(row + 3, col, color);
    led_matrix__clearPixel(row + 3, col + 1);
    led_matrix__clearPixel(row + 3, col + 2);
    led_matrix__drawPixel(row + 3, col + 3, color);
    // row 4
    led_matrix__clearPixel(row + 4, col);
    led_matrix__drawPixel(row + 4, col + 1, color);
    led_matrix__drawPixel(row + 4, col + 2, color);
    led_matrix__clearPixel(row + 4, col + 3);
    break;

  case 'V':
    // row 0
    led_matrix__drawPixel(row, col, color);
    led_matrix__clearPixel(row, col + 1);
    led_matrix__clearPixel(row, col + 2);
    led_matrix__drawPixel(row, col + 3, color);
    // row 1
    led_matrix__drawPixel(row + 1, col, color);
    led_matrix__clearPixel(row + 1, col + 1);
    led_matrix__clearPixel(row + 1, col + 2);
    led_matrix__drawPixel(row + 1, col + 3, color);
    // row 2
    led_matrix__drawPixel(row + 2, col, color);
    led_matrix__clearPixel(row + 2, col + 1);
    led_matrix__clearPixel(row + 2, col + 2);
    led_matrix__drawPixel(row + 2, col + 3, color);
    // row 3
    led_matrix__drawPixel(row + 3, col, color);
    led_matrix__clearPixel(row + 3, col + 1);
    led_matrix__drawPixel(row + 3, col + 2, color);
    led_matrix__clearPixel(row + 3, col + 3);
    // row 4
    led_matrix__clearPixel(row + 4, col);
    led_matrix__drawPixel(row + 4, col + 1, color);
    led_matrix__clearPixel(row + 4, col + 2);
    led_matrix__clearPixel(row + 4, col + 3);
    break;

  case 'W':
    // row 0
    led_matrix__drawPixel(row, col, color);
    led_matrix__clearPixel(row, col + 1);
    led_matrix__clearPixel(row, col + 2);
    led_matrix__clearPixel(row, col + 3);
    led_matrix__drawPixel(row, col + 4, color);
    // row 1
    led_matrix__drawPixel(row + 1, col, color);
    led_matrix__clearPixel(row + 1, col + 1);
    led_matrix__clearPixel(row + 1, col + 2);
    led_matrix__clearPixel(row + 1, col + 3);
    led_matrix__drawPixel(row + 1, col + 4, color);
    // row 2
    led_matrix__drawPixel(row + 2, col, color);
    led_matrix__clearPixel(row + 2, col + 1);
    led_matrix__drawPixel(row + 2, col + 2, color);
    led_matrix__clearPixel(row + 2, col + 3);
    led_matrix__drawPixel(row + 2, col + 4, color);
    // row 3
    led_matrix__drawPixel(row + 3, col, color);
    led_matrix__clearPixel(row + 3, col + 1);
    led_matrix__drawPixel(row + 3, col + 2, color);
    led_matrix__clearPixel(row + 3, col + 3);
    led_matrix__drawPixel(row + 3, col + 4, color);
    // row 4
    led_matrix__clearPixel(row + 4, col);
    led_matrix__drawPixel(row + 4, col + 1, color);
    led_matrix__clearPixel(row + 4, col + 2);
    led_matrix__drawPixel(row + 4, col + 3, color);
    led_matrix__clearPixel(row + 4, col + 4);

    width = 5;
    break;

  case 'X':
    // row 0
    led_matrix__drawPixel(row, col, color);
    led_matrix__clearPixel(row, col + 1);
    led_matrix__clearPixel(row, col + 2);
    led_matrix__drawPixel(row, col + 3, color);
    // row 1
    led_matrix__drawPixel(row + 1, col, color);
    led_matrix__drawPixel(row + 1, col + 1, color);
    led_matrix__clearPixel(row + 1, col + 2);
    led_matrix__drawPixel(row + 1, col + 3, color);
    // row 2
    led_matrix__clearPixel(row + 2, col);
    led_matrix__drawPixel(row + 2, col + 1, color);
    led_matrix__drawPixel(row + 2, col + 2, color);
    led_matrix__clearPixel(row + 2, col + 3);
    // row 3
    led_matrix__drawPixel(row + 3, col, color);
    led_matrix__clearPixel(row + 3, col + 1);
    led_matrix__drawPixel(row + 3, col + 2, color);
    led_matrix__drawPixel(row + 3, col + 3, color);
    // row 4
    led_matrix__drawPixel(row + 4, col, color);
    led_matrix__clearPixel(row + 4, col + 1);
    led_matrix__clearPixel(row + 4, col + 2);
    led_matrix__drawPixel(row + 4, col + 3, color);
    break;

  case 'Y':
    // row 0
    led_matrix__drawPixel(row, col, color);
    led_matrix__clearPixel(row, col + 1);
    led_matrix__clearPixel(row, col + 2);
    led_matrix__drawPixel(row, col + 3, color);
    // row 1
    led_matrix__drawPixel(row + 1, col, color);
    led_matrix__clearPixel(row + 1, col + 1);
    led_matrix__clearPixel(row + 1, col + 2);
    led_matrix__drawPixel(row + 1, col + 3, color);
    // row 2
    led_matrix__drawPixel(row + 2, col, color);
    led_matrix__drawPixel(row + 2, col + 1, color);
    led_matrix__drawPixel(row + 2, col + 2, color);
    led_matrix__clearPixel(row + 2, col + 3);
    // row 3
    led_matrix__clearPixel(row + 3, col);
    led_matrix__clearPixel(row + 3, col + 1);
    led_matrix__drawPixel(row + 3, col + 2, color);
    led_matrix__clearPixel(row + 3, col + 3);
    // row 4
    led_matrix__clearPixel(row + 4, col);
    led_matrix__drawPixel(row + 4, col + 1, color);
    led_matrix__clearPixel(row + 4, col + 2);
    led_matrix__clearPixel(row + 4, col + 3);
    break;

  case 'Z':
    // row 0
    led_matrix__drawPixel(row, col, color);
    led_matrix__drawPixel(row, col + 1, color);
    led_matrix__drawPixel(row, col + 2, color);
    led_matrix__drawPixel(row, col + 3, color);
    // row 1
    led_matrix__clearPixel(row + 1, col);
    led_matrix__clearPixel(row + 1, col + 1);
    led_matrix__clearPixel(row + 1, col + 2);
    led_matrix__drawPixel(row + 1, col + 3, color);
    // row 2
    led_matrix__clearPixel(row + 2, col);
    led_matrix__drawPixel(row + 2, col + 1, color);
    led_matrix__drawPixel(row + 2, col + 2, color);
    led_matrix__clearPixel(row + 2, col + 3);
    // row 3
    led_matrix__drawPixel(row + 3, col, color);
    led_matrix__clearPixel(row + 3, col + 1);
    led_matrix__clearPixel(row + 3, col + 2);
    led_matrix__clearPixel(row + 3, col + 3);
    // row 4
    led_matrix__drawPixel(row + 4, col, color);
    led_matrix__drawPixel(row + 4, col + 1, color);
    led_matrix__drawPixel(row + 4, col + 2, color);
    led_matrix__drawPixel(row + 4, col + 3, color);
    break;

  case '!':
    // row 0
    led_matrix__drawPixel(row, col, color);
    // row 1
    led_matrix__drawPixel(row + 1, col, color);
    // row 2
    led_matrix__drawPixel(row + 2, col, color);
    // row 3
    led_matrix__clearPixel(row + 3, col);
    // row 4
    led_matrix__drawPixel(row + 4, col, color);
    width = 1;
    break;

  case ':':
    // row 0
    led_matrix__clearPixel(row, col);
    // row 1
    led_matrix__drawPixel(row + 1, col, color);
    // row 2
    led_matrix__clearPixel(row + 2, col);
    // row 3
    led_matrix__drawPixel(row + 3, col, color);
    // row 4
    led_matrix__clearPixel(row + 4, col);
    width = 1;
    break;

  default: // draw nothing, no width
    width = 0;
  }
  return width;
}

void draw_start_screen(int current_difficulty, controller_selection selection) {
  // per row basis
  // row 0
  int row = 0;
  for (int col = 0; col < 64; col++) {
    if (col < 5 || (col >= 12 && col <= 53) || col >= 62)
      led_matrix__clearPixel(row, col);
    else if (col == 5 || col == 11 || col == 54 || col == 61) {
      led_matrix__drawPixel(row, col, WHITE);
    } else if (col >= 6 && col <= 10) {
      led_matrix__drawPixel(row, col, YELLOW);
    } else if (col >= 55 && col <= 60) {
      led_matrix__drawPixel(row, col, GREEN);
    }
  }

  // row 1
  row = 1;
  for (int col = 0; col < 64; col++) {
    if (col < 5 || (col >= 12 && col <= 53) || col >= 62)
      led_matrix__clearPixel(row, col);
    else if (col == 5 || col == 11 || col == 54 || col == 61) {
      led_matrix__drawPixel(row, col, WHITE);
    } else if (col >= 6 && col <= 10) {
      led_matrix__drawPixel(row, col, YELLOW);
    } else if (col >= 55 && col <= 60) {
      led_matrix__drawPixel(row, col, GREEN);
    }
  }

  // row 2
  row = 2;
  for (int col = 0; col < 64; col++) {
    if (col < 5 || (col >= 12 && col <= 52) || col >= 61)
      led_matrix__clearPixel(row, col);
    else if (col == 5 || col == 11 || col == 53 || col == 60) {
      led_matrix__drawPixel(row, col, WHITE);
    } else if (col >= 6 && col <= 10) {
      led_matrix__drawPixel(row, col, YELLOW);
    } else if (col >= 54 && col <= 59) {
      led_matrix__drawPixel(row, col, GREEN);
    }
  }

  // row 3
  row = 3;
  for (int col = 0; col < 64; col++) {
    if (col < 5 || (col >= 12 && col <= 17) || (col >= 19 && col <= 22) || (col >= 24 && col <= 52) || col >= 61)
      led_matrix__clearPixel(row, col);
    else if (col == 5 || col == 11 || col == 53 || col == 60) {
      led_matrix__drawPixel(row, col, WHITE);
    } else if ((col >= 7 && col <= 9) || col == 56) {
      led_matrix__drawPixel(row, col, YELLOW);
    } else if (col == 6 || col == 10 || (col >= 54 && col <= 55) || (col >= 57 && col <= 59)) {
      led_matrix__drawPixel(row, col, GREEN);
    } else if (col == 18 || col == 23) {
      led_matrix__drawPixel(row, col, CYAN);
    }
  }

  // row 4
  row = 4;
  for (int col = 0; col < 64; col++) {
    if (col < 5 || (col >= 12 && col <= 17) || (col >= 19 && col <= 22) || (col >= 24 && col <= 52) || col >= 61)
      led_matrix__clearPixel(row, col);
    else if (col == 5 || col == 11 || col == 53 || col == 60) {
      led_matrix__drawPixel(row, col, WHITE);
    } else if ((col >= 7 && col <= 9) || col == 55 || col == 56 || col == 57) {
      led_matrix__drawPixel(row, col, YELLOW);
    } else if ((col >= 6 && col <= 7) || col == 9 || col == 10 || col == 54 || (col >= 58 && col <= 59)) {
      led_matrix__drawPixel(row, col, GREEN);
    } else if (col == 18 || col == 23) {
      led_matrix__drawPixel(row, col, CYAN);
    }
  }

  // row 5
  row = 5;
  for (int col = 0; col < 64; col++) {
    if (col < 5 || (col >= 12 && col <= 17) || (col >= 19 && col <= 22) || (col >= 24 && col <= 53) || col >= 62)
      led_matrix__clearPixel(row, col);
    else if (col == 5 || col == 11 || col == 54 || col == 61) {
      led_matrix__drawPixel(row, col, WHITE);
    } else if ((col >= 55 && col <= 58) || col == 60) {
      led_matrix__drawPixel(row, col, YELLOW);
    } else if ((col >= 6 && col <= 10) || col == 59) {
      led_matrix__drawPixel(row, col, GREEN);
    } else if (col == 18 || col == 23) {
      led_matrix__drawPixel(row, col, CYAN);
    }
  }

  // row 6
  row = 6;
  for (int col = 0; col < 64; col++) {
    if (col < 5 || (col >= 12 && col <= 17) || (col >= 19 && col <= 22) || (col >= 24 && col <= 53) || col >= 62)
      led_matrix__clearPixel(row, col);
    else if (col == 5 || col == 11 || col == 54 || col == 61) {
      led_matrix__drawPixel(row, col, WHITE);
    } else if (col >= 55 && col <= 60) {
      led_matrix__drawPixel(row, col, YELLOW);
    } else if ((col >= 6 && col <= 10)) {
      led_matrix__drawPixel(row, col, GREEN);
    } else if (col == 18 || col == 23) {
      led_matrix__drawPixel(row, col, CYAN);
    }
  }

  // row 7
  row = 7;
  for (int col = 0; col < 64; col++) {
    if (col < 5 || (col >= 12 && col <= 17) || (col >= 19 && col <= 22) || (col >= 24 && col <= 53) || col >= 62)
      led_matrix__clearPixel(row, col);
    else if (col == 5 || col == 11 || col == 53 || col == 60) {
      led_matrix__drawPixel(row, col, WHITE);
    } else if (col >= 54 && col <= 59) {
      led_matrix__drawPixel(row, col, YELLOW);
    } else if ((col >= 6 && col <= 10)) {
      led_matrix__drawPixel(row, col, GREEN);
    } else if (col == 18 || col == 23) {
      led_matrix__drawPixel(row, col, CYAN);
    }
  }

  // row 8
  row = 8;
  for (int col = 0; col < 64; col++) {
    if (col < 5 || (col >= 12 && col <= 17) || col == 24 || col == 26 || col == 27 || col == 29 || col == 30 ||
        (col >= 33 && col <= 35) || col == 39 || col == 41 || col == 42 || col == 44 || col == 46 || col == 47 ||
        (col >= 49 && col <= 52) || col >= 61)
      led_matrix__clearPixel(row, col);
    else if (col == 5 || col == 11 || col == 53 || col == 60) {
      led_matrix__drawPixel(row, col, WHITE);
    } else if (col == 6 || col == 10 || (col >= 54 && col <= 59)) {
      led_matrix__drawPixel(row, col, YELLOW);
    } else if (col >= 7 && col <= 9) {
      led_matrix__drawPixel(row, col, GREEN);
    } else if ((col >= 18 && col <= 23) || col == 25 || col == 28 || col == 31 || col == 32 ||
               (col >= 36 && col <= 38) || col == 40 || col == 43 || col == 45 || col == 48) {
      led_matrix__drawPixel(row, col, CYAN);
    }
  }

  // row 9
  row = 9;
  for (int col = 0; col < 64; col++) {
    if (col < 4 || (col >= 12 && col <= 17) || (col >= 19 && col <= 22) || col == 24 || col == 26 || col == 27 ||
        col == 29 || (col >= 31 && col <= 32) || col == 34 || col == 36 || col == 37 || col == 39 || col == 41 ||
        col == 43 || col == 44 || col == 46 || col == 47 || (col >= 49 && col <= 52) || col >= 61)
      led_matrix__clearPixel(row, col);
    else if (col == 5 || col == 11 || col == 53 || col == 60) {
      led_matrix__drawPixel(row, col, WHITE);
    } else if (col == 6 || col == 7 || col == 9 || col == 10 || (col >= 54 && col <= 55) || (col >= 57 && col <= 59)) {
      led_matrix__drawPixel(row, col, YELLOW);
    } else if (col == 8 || col == 56) {
      led_matrix__drawPixel(row, col, GREEN);
    } else if (col == 18 || col == 23 || col == 25 || col == 28 || col == 30 || col == 33 || col == 35 || col == 38 ||
               col == 40 || col == 42 || col == 45 || col == 48) {
      led_matrix__drawPixel(row, col, CYAN);
    }
  }

  // row 10
  row = 10;
  for (int col = 0; col < 64; col++) {
    if (col < 4 || (col >= 13 && col <= 17) || (col >= 19 && col <= 22) || col == 24 || col == 26 || col == 27 ||
        col == 29 || (col >= 31 && col <= 32) || col == 34 || col == 36 || col == 37 || col == 39 || col == 43 ||
        col == 44 || col == 46 || col == 47 || (col >= 49 && col <= 52) || col >= 61)
      led_matrix__clearPixel(row, col);
    else if (col == 4 || col == 12 || col == 53 || col == 60) {
      led_matrix__drawPixel(row, col, WHITE);
    } else if ((col >= 5 && col <= 11) || col == 54 || (col >= 58 && col <= 59)) {
      led_matrix__drawPixel(row, col, YELLOW);
    } else if ((col >= 55 && col <= 57)) {
      led_matrix__drawPixel(row, col, GREEN);
    } else if (col == 18 || col == 23 || col == 25 || col == 28 || col == 30 || col == 33 || col == 35 || col == 38 ||
               (col >= 40 && col <= 43) || col == 45 || col == 48) {
      led_matrix__drawPixel(row, col, CYAN);
    }
  }

  // row 11
  row = 11;
  for (int col = 0; col < 64; col++) {
    if (col < 3 || (col >= 14 && col <= 17) || (col >= 19 && col <= 22) || col == 24 || col == 26 || col == 27 ||
        col == 29 || (col >= 31 && col <= 32) || col == 34 || col == 36 || col == 37 || col == 39 ||
        (col >= 43 && col <= 44) || col == 46 || col == 47 || (col >= 49 && col <= 51) || col >= 60)
      led_matrix__clearPixel(row, col);
    else if (col == 3 || col == 13 || col == 52 || col == 59) {
      led_matrix__drawPixel(row, col, WHITE);
    } else if ((col >= 4 && col <= 12) || col == 53 || col == 58) {
      led_matrix__drawPixel(row, col, YELLOW);
    } else if ((col >= 54 && col <= 57)) {
      led_matrix__drawPixel(row, col, GREEN);
    } else if (col == 18 || col == 23 || col == 25 || col == 28 || col == 30 || col == 33 || col == 35 || col == 38 ||
               col == 40 || col == 45 || col == 48) {
      led_matrix__drawPixel(row, col, CYAN);
    }
  }

  // row 12
  row = 12;
  for (int col = 0; col < 64; col++) {
    if (col < 3 || (col >= 14 && col <= 17) || (col >= 19 && col <= 22) || col == 24 || col == 26 || col == 27 ||
        col == 29 || (col >= 31 && col <= 32) || col == 34 || col == 35 || col == 39 || (col >= 41 && col <= 45) ||
        (col >= 49 && col <= 51) || col >= 60)
      led_matrix__clearPixel(row, col);
    else if (col == 3 || col == 13 || col == 52 || col == 59) {
      led_matrix__drawPixel(row, col, WHITE);
    } else if ((col >= 4 && col <= 12)) {
      led_matrix__drawPixel(row, col, YELLOW);
    } else if ((col >= 53 && col <= 58)) {
      led_matrix__drawPixel(row, col, GREEN);
    } else if (col == 18 || col == 23 || col == 25 || col == 28 || col == 30 || col == 33 || (col >= 36 && col <= 38) ||
               col == 38 || col == 40 || (col >= 46 && col <= 48)) {
      led_matrix__drawPixel(row, col, CYAN);
    }
  }

  // row 13
  row = 13;
  for (int col = 0; col < 64; col++) {
    if (col < 2 || (col >= 15 && col <= 17) || (col >= 19 && col <= 22) || col == 24 || col == 25 || col == 28 ||
        col == 29 || (col >= 34 && col <= 37) || col == 39 || (col >= 41 && col <= 47) || (col >= 49 && col <= 51) ||
        col >= 59)
      led_matrix__clearPixel(row, col);
    else if (col == 2 || col == 14 || col == 51 || col == 58) {
      led_matrix__drawPixel(row, col, WHITE);
    } else if ((col >= 4 && col <= 11)) {
      led_matrix__drawPixel(row, col, YELLOW);
    } else if (col == 3 || col == 12 || col == 13 || (col >= 52 && col <= 57)) {
      led_matrix__drawPixel(row, col, GREEN);
    } else if (col == 18 || col == 23 || col == 26 || col == 27 || col == 30 || col == 33 || col == 38 || col == 40 ||
               col == 48) {
      led_matrix__drawPixel(row, col, CYAN);
    }
  }

  // row 14
  row = 14;
  for (int col = 0; col < 64; col++) {
    if (col < 2 || (col >= 15 && col <= 37) || (col >= 39 && col <= 47) || (col >= 49 && col <= 50) || col >= 59)
      led_matrix__clearPixel(row, col);
    else if (col == 2 || col == 14 || col == 51 || col == 58) {
      led_matrix__drawPixel(row, col, WHITE);
    } else if ((col >= 6 && col <= 9)) {
      led_matrix__drawPixel(row, col, YELLOW);
    } else if ((col >= 3 && col <= 5) || (col >= 10 && col <= 13) || (col >= 52 && col <= 57)) {
      led_matrix__drawPixel(row, col, GREEN);
    } else if (col == 38 || col == 48) {
      led_matrix__drawPixel(row, col, CYAN);
    }
  }

  // row 15
  row = 15;
  for (int col = 0; col < 64; col++) {
    if (col == 0 || (col >= 16 && col <= 34) || col == 36 || col == 37 || (col >= 39 && col <= 47) ||
        (col >= 49 && col <= 51) || col >= 60)
      led_matrix__clearPixel(row, col);
    else if (col == 1 || col == 15 || col == 52 || col == 59) {
      led_matrix__drawPixel(row, col, WHITE);
    } else if ((col >= 7 && col <= 8)) {
      led_matrix__drawPixel(row, col, YELLOW);
    } else if ((col >= 2 && col <= 6) || (col >= 9 && col <= 14) || (col >= 53 && col <= 58)) {
      led_matrix__drawPixel(row, col, GREEN);
    } else if (col == 35 || col == 38 || col == 48) {
      led_matrix__drawPixel(row, col, CYAN);
    }
  }

  // row 16
  row = 16;
  for (int col = 0; col < 64; col++) {
    if (col == 0 || (col >= 16 && col <= 19) || (col >= 23 && col <= 34) || col == 36 || col == 37 ||
        (col >= 39 && col <= 44) || col == 46 || col == 47 || (col >= 49 && col <= 51) || col >= 60)
      led_matrix__clearPixel(row, col);
    else if (col == 1 || col == 15 || col == 52 || col == 59) {
      led_matrix__drawPixel(row, col, WHITE);
    } else if ((col >= 2 && col <= 14) || (col >= 53 && col <= 58)) {
      led_matrix__drawPixel(row, col, GREEN);
    } else if ((col >= 20 && col <= 22) || col == 35 || col == 38 || col == 45 || col == 48) {
      led_matrix__drawPixel(row, col, CYAN);
    }
  }

  // row 17
  row = 17;
  for (int col = 0; col < 64; col++) {
    if (col == 0 || (col >= 16 && col <= 18) || (col >= 23 && col <= 34) || (col >= 39 && col <= 44) ||
        (col >= 49 && col <= 51) || col >= 61)
      led_matrix__clearPixel(row, col);
    else if (col == 1 || col == 15 || col == 53 || col == 60) {
      led_matrix__drawPixel(row, col, WHITE);
    } else if ((col >= 2 && col <= 14) || (col >= 54 && col <= 59)) {
      led_matrix__drawPixel(row, col, GREEN);
    } else if (col == 19 || col == 23 || col == 36 || col == 37 || col == 46 || col == 47) {
      led_matrix__drawPixel(row, col, CYAN);
    }
  }

  // row 18
  row = 18;
  for (int col = 0; col < 64; col++) {
    if (col == 0 || (col >= 16 && col <= 18) || (col >= 20 && col <= 52))
      led_matrix__clearPixel(row, col);
    else if (col == 1 || col == 15 || col == 53 || col == 60) {
      led_matrix__drawPixel(row, col, WHITE);
    } else if (col == 57) {
      led_matrix__drawPixel(row, col, YELLOW);
    } else if ((col >= 2 && col <= 14) || (col >= 54 && col <= 59)) {
      led_matrix__drawPixel(row, col, GREEN);
    } else if (col == 19) {
      led_matrix__drawPixel(row, col, CYAN);
    }
  }

  // row 19
  row = 19;
  for (int col = 0; col < 64; col++) {
    if (col == 0 || (col >= 16 && col <= 18) || (col >= 20 && col <= 31) || (col >= 33 && col <= 35) ||
        (col >= 37 && col <= 53) || col >= 62)
      led_matrix__clearPixel(row, col);
    else if (col == 1 || col == 15 || col == 54 || col == 61) {
      led_matrix__drawPixel(row, col, WHITE);
    } else if (col == 56 || col == 57) {
      led_matrix__drawPixel(row, col, YELLOW);
    } else if ((col >= 2 && col <= 14) || col == 32 || col == 55 || (col >= 58 && col <= 60)) {
      led_matrix__drawPixel(row, col, GREEN);
    } else if (col == 19 || col == 36) {
      led_matrix__drawPixel(row, col, CYAN);
    }
  }

  // row 20
  row = 20;
  for (int col = 0; col < 64; col++) {
    if (col == 0 || (col >= 16 && col <= 18) || (col >= 20 && col <= 31) || (col >= 33 && col <= 35) ||
        (col >= 37 && col <= 38) || col == 40 || col == 41 || (col >= 44 && col <= 53) || col >= 62)
      led_matrix__clearPixel(row, col);
    else if (col == 1 || col == 4 || col == 11 || col == 15 || col == 54 || col == 61) {
      led_matrix__drawPixel(row, col, WHITE);
    } else if (col >= 55 && col <= 59) {
      led_matrix__drawPixel(row, col, YELLOW);
    } else if ((col >= 2 && col <= 3) || (col >= 6 && col <= 10) || col == 13 || col == 14 || col == 32 || col == 60) {
      led_matrix__drawPixel(row, col, GREEN);
    } else if (col == 19 || col == 36 || col == 39 || col == 42 || col == 43) {
      led_matrix__drawPixel(row, col, CYAN);
    }
  }

  // row 21
  row = 21;
  for (int col = 0; col < 64; col++) {
    if (col == 0 || (col >= 16 && col <= 19) || (col >= 23 && col <= 25) || (col >= 28 && col <= 30) ||
        (col >= 34 && col <= 35) || col == 37 || col == 39 || col == 40 || col == 42 || col == 43 ||
        (col >= 45 && col <= 54) || col >= 62)
      led_matrix__clearPixel(row, col);
    else if (col == 1 || col == 15 || col == 55 || col == 61) {
      led_matrix__drawPixel(row, col, WHITE);
    } else if (col >= 56 && col <= 59) {
      led_matrix__drawPixel(row, col, YELLOW);
    } else if ((col >= 2 && col <= 3) || (col >= 6 && col <= 10) || col == 13 || col == 14 || col == 60) {
      led_matrix__drawPixel(row, col, GREEN);
    } else if ((col >= 20 && col <= 22) || col == 26 || col == 27 || col == 36 || col == 38 || col == 41 || col == 44) {
      led_matrix__drawPixel(row, col, CYAN);
    } else if (col >= 31 && col <= 33) {
      led_matrix__drawPixel(row, col, RED);
    }
  }

  // row 22
  row = 22;
  for (int col = 0; col < 64; col++) {
    if (col == 0 || (col >= 16 && col <= 22) || col == 24 || (col >= 26 && col <= 27) || col == 29 || col == 35 ||
        (col >= 38 && col <= 40) || col == 40 || col == 42 || col == 43 || (col >= 45 && col <= 54) || col >= 62)
      led_matrix__clearPixel(row, col);
    else if (col == 1 || col == 15 || col == 33 || col == 55 || col == 61) {
      led_matrix__drawPixel(row, col, WHITE);
    } else if (col >= 56 && col <= 60) {
      led_matrix__drawPixel(row, col, YELLOW);
    } else if ((col >= 2 && col <= 3) || (col >= 6 && col <= 10) || col == 13 || col == 14) {
      led_matrix__drawPixel(row, col, GREEN);
    } else if (col == 23 || col == 25 || col == 28 || col == 36 || col == 37 || col == 41 || col == 44) {
      led_matrix__drawPixel(row, col, CYAN);
    } else if ((col >= 30 && col <= 32) || col == 34) {
      led_matrix__drawPixel(row, col, RED);
    }
  }

  // row 23
  row = 23;
  for (int col = 0; col < 64; col++) {
    if (col == 0 || (col >= 16 && col <= 22) || col == 24 || (col >= 26 && col <= 27) || col == 29 || col == 35 ||
        col == 37 || (col >= 39 && col <= 40) || (col >= 44 && col <= 53) || col >= 62)
      led_matrix__clearPixel(row, col);
    else if (col == 1 || col == 15 || col == 54 || col == 61) {
      led_matrix__drawPixel(row, col, WHITE);
    } else if (col >= 55 && col <= 60) {
      led_matrix__drawPixel(row, col, YELLOW);
    } else if ((col >= 2 && col <= 3) || (col >= 6 && col <= 10) || col == 13 || col == 14) {
      led_matrix__drawPixel(row, col, GREEN);
    } else if (col == 23 || col == 25 || col == 28 || col == 36 || col == 38 || (col >= 41 && col <= 43)) {
      led_matrix__drawPixel(row, col, CYAN);
    } else if (col >= 30 && col <= 34) {
      led_matrix__drawPixel(row, col, RED);
    }
  }

  // row 24
  row = 24;
  for (int col = 0; col < 64; col++) {
    if (col == 0 || (col >= 16 && col <= 22) || col == 24 || (col >= 26 && col <= 27) || col == 29 || col == 35 ||
        col == 37 || col == 38 || col == 40 || (col >= 42 && col <= 53) || col >= 62)
      led_matrix__clearPixel(row, col);
    else if (col == 1 || col == 15 || col == 33 || col == 54 || col == 61) {
      led_matrix__drawPixel(row, col, WHITE);
    } else if (col >= 55 && col <= 60) {
      led_matrix__drawPixel(row, col, YELLOW);
    } else if ((col >= 2 && col <= 14)) {
      led_matrix__drawPixel(row, col, GREEN);
    } else if (col == 23 || col == 25 || col == 28 || col == 36 || col == 39 || col == 41) {
      led_matrix__drawPixel(row, col, CYAN);
    } else if (col >= 30 && col <= 34) {
      led_matrix__drawPixel(row, col, RED);
    }
  }

  // row 25
  row = 25;
  for (int col = 0; col < 64; col++) {
    if (col < 2 || (col >= 15 && col <= 18) || (col >= 20 && col <= 22) || col == 24 || (col >= 26 && col <= 27) ||
        col == 29 || col == 35 || col == 37 || col == 38 || col == 40 || (col >= 42 && col <= 43) ||
        (col >= 45 && col <= 54) || col >= 61)
      led_matrix__clearPixel(row, col);
    else if (col == 2 || col == 3 || col == 13 || col == 14 || col == 33 || col == 55 || col == 60) {
      led_matrix__drawPixel(row, col, WHITE);
    } else if (col >= 56 && col <= 59) {
      led_matrix__drawPixel(row, col, YELLOW);
    } else if ((col >= 4 && col <= 12)) {
      led_matrix__drawPixel(row, col, GREEN);
    } else if (col == 19 || col == 23 || col == 25 || col == 28 || col == 36 || col == 39 || col == 41 || col == 44) {
      led_matrix__drawPixel(row, col, CYAN);
    } else if (col >= 30 && col <= 34) {
      led_matrix__drawPixel(row, col, RED);
    }
  }

  // row 26
  row = 26;
  for (int col = 0; col < 64; col++) {
    if (col < 3 || (col >= 14 && col <= 19) || (col >= 23 && col <= 24) || (col >= 26 && col <= 27) || col == 29 ||
        col == 35 || col == 30 || col == 34 || col == 35 || col == 37 || col == 38 || col == 40 || col == 41 ||
        (col >= 44 && col <= 54))
      led_matrix__clearPixel(row, col);
    else if (col == 3 || col == 13 || col == 55 || col == 60) {
      led_matrix__drawPixel(row, col, WHITE);
    } else if (col >= 56 && col <= 59) {
      led_matrix__drawPixel(row, col, YELLOW);
    } else if ((col >= 4 && col <= 12)) {
      led_matrix__drawPixel(row, col, GREEN);
    } else if ((col >= 20 && col <= 22) || col == 25 || col == 28 || col == 36 || col == 39 || col == 42 || col == 43) {
      led_matrix__drawPixel(row, col, CYAN);
    } else if (col >= 31 && col <= 33) {
      led_matrix__drawPixel(row, col, RED);
    }
  }

  // row 27
  row = 27;
  for (int col = 0; col < 64; col++) {
    if (col < 3 || (col >= 14 && col <= 53) || col >= 60)
      led_matrix__clearPixel(row, col);
    else if (col == 3 || col == 13 || col == 54 || col == 59) {
      led_matrix__drawPixel(row, col, WHITE);
    } else if (col >= 55 && col <= 58) {
      led_matrix__drawPixel(row, col, YELLOW);
    } else if ((col >= 4 && col <= 12)) {
      led_matrix__drawPixel(row, col, GREEN);
    }
  }

  // row 28
  row = 28;
  for (int col = 0; col < 64; col++) {
    if (col < 4 || (col >= 13 && col <= 53) || col >= 60)
      led_matrix__clearPixel(row, col);
    else if (col == 4 || col == 5 || col == 11 || col == 12 || col == 54 || col == 59) {
      led_matrix__drawPixel(row, col, WHITE);
    } else if (col >= 55 && col <= 58) {
      led_matrix__drawPixel(row, col, YELLOW);
    } else if ((col >= 6 && col <= 10)) {
      led_matrix__drawPixel(row, col, GREEN);
    }
  }

  // row 29
  row = 29;
  for (int col = 0; col < 64; col++) {
    if (col < 6 || (col >= 11 && col <= 54) || col >= 59)
      led_matrix__clearPixel(row, col);
    else if (col == 6 || col == 10 || col == 55 || col == 58) {
      led_matrix__drawPixel(row, col, WHITE);
    } else if (col >= 56 && col <= 57) {
      led_matrix__drawPixel(row, col, YELLOW);
    } else if ((col >= 7 && col <= 9)) {
      led_matrix__drawPixel(row, col, GREEN);
    }
  }

  // row 30
  row = 30;
  for (int col = 0; col < 64; col++) {
    if (col < 7 || (col >= 10 && col <= 54) || col >= 58)
      led_matrix__clearPixel(row, col);
    else if ((col >= 7 && col <= 9) || col == 55 || col == 57) {
      led_matrix__drawPixel(row, col, WHITE);
    } else if (col == 56) {
      led_matrix__drawPixel(row, col, YELLOW);
    }
  }

  // row 31
  row = 31;
  for (int col = 0; col < 64; col++) {
    if (col <= 7 || (col >= 9 && col <= 11) || (col >= 13 && col <= 15) || (col >= 17 && col <= 55) || col >= 57)
      led_matrix__clearPixel(row, col);
    else if (col == 8 || col == 12 || col == 16 || col == 56) {
      led_matrix__drawPixel(row, col, WHITE);
    }
  }

  // row 32
  row = 32;
  for (int col = 0; col < 64; col++) {
    if (col <= 7 || (col >= 9 && col <= 12) || col >= 16)
      led_matrix__clearPixel(row, col);
    else if (col == 8 || col == 13 || col == 15) {
      led_matrix__drawPixel(row, col, WHITE);
    }
  }

  // row 33
  row = 33;
  for (int col = 0; col < 64; col++) {
    if (col <= 7 || (col >= 9 && col <= 13) || col >= 15)
      led_matrix__clearPixel(row, col);
    else if (col == 8 || col == 14) {
      led_matrix__drawPixel(row, col, WHITE);
    }
  }

  // row 34
  row = 34;
  for (int col = 0; col < 64; col++) {
    if (col <= 7 || (col >= 9 && col <= 13) || col >= 15)
      led_matrix__clearPixel(row, col);
    else if (col == 8 || col == 14) {
      led_matrix__drawPixel(row, col, WHITE);
    }
  }

  // row 35
  row = 35;
  for (int col = 0; col < 64; col++) {
    if (col <= 8 || (col >= 10 && col <= 12) || col >= 14)
      led_matrix__clearPixel(row, col);
    else if (col == 9 || col == 13) {
      led_matrix__drawPixel(row, col, WHITE);
    }
  }

  // row 36
  row = 36;
  for (int col = 0; col < 64; col++) {
    if (col <= 9 || col >= 13)
      led_matrix__clearPixel(row, col);
    else if (col >= 10 && col <= 12) {
      led_matrix__drawPixel(row, col, WHITE);
    }
  }

  // difficulty selection (starting at (39, 6))
  char *difficulty_string = "DIFFICULTY: ";
  // which selection is the user selected (either difficulty, or start)
  if (selection == difficulty) {
    draw_string(39, 6, difficulty_string, 12, CYAN);
  } else {
    draw_string(39, 6, difficulty_string, 12, BLUE);
  }

  // clear all apple pixels
  for (int row = 45; row < 52; row++) {
    for (int col = 0; col < 63; col++) {
      led_matrix__clearPixel(row, col);
    }
  }
  if (current_difficulty == 0) // easy
  {
    // display one apple (row 45) easy difficulty
    draw_start_screen_fruit(45, 30);
  }

  else if (current_difficulty == 1) // medium
  {
    // display 2 apples (row 45)
    draw_start_screen_fruit(45, 25);
    draw_start_screen_fruit(45, 35);
  } else if (current_difficulty == 2) // hard
  {
    // display 3 apples
    draw_start_screen_fruit(45, 22);
    draw_start_screen_fruit(45, 30);
    draw_start_screen_fruit(45, 38);
  } else // display secret level
  {
    draw_start_screen_secret(45, 30);
  }

  // letters for selection (starting at (54, 19))
  char *start_string = "START";
  if (selection == start) {
    draw_string(54, 19, start_string, 5, CYAN);
  } else {
    draw_string(54, 19, start_string, 5, BLUE);
  }
}

void draw_start_screen_secret(int start_row, int start_col) {
  for (int col = start_col; col < start_col + 4; col++) {
    if (col == start_col) {
      led_matrix__drawPixel(start_row + 1, col, GREEN);
    } else if (col == start_col + 1) {
      led_matrix__drawPixel(start_row, col, GREEN);
      led_matrix__drawPixel(start_row + 3, col, GREEN);
      led_matrix__drawPixel(start_row + 4, col, GREEN);
      led_matrix__drawPixel(start_row + 6, col, GREEN);
    } else if (col == start_col + 2) {
      led_matrix__drawPixel(start_row, col, GREEN);
      led_matrix__drawPixel(start_row + 3, col, GREEN);
    } else if (col == start_col + 3) {
      led_matrix__drawPixel(start_row, col, GREEN);
      led_matrix__drawPixel(start_row + 1, col, GREEN);
      led_matrix__drawPixel(start_row + 2, col, GREEN);
      led_matrix__drawPixel(start_row + 3, col, GREEN);
    }
  }
}

void draw_start_screen_fruit(int start_row, int start_col) {
  for (int col = start_col; col < start_col + 5; col++) {
    if (col == start_col) {
      led_matrix__drawPixel(start_row + 3, col, RED);
      led_matrix__drawPixel(start_row + 4, col, RED);
      led_matrix__drawPixel(start_row + 5, col, RED);
    } else if (col == start_col + 1) {
      led_matrix__drawPixel(start_row + 2, col, RED);
      led_matrix__drawPixel(start_row + 3, col, RED);
      led_matrix__drawPixel(start_row + 4, col, RED);
      led_matrix__drawPixel(start_row + 5, col, RED);
      led_matrix__drawPixel(start_row + 6, col, RED);
    } else if (col == start_col + 2) {
      led_matrix__drawPixel(start_row, col, GREEN);
      led_matrix__drawPixel(start_row + 1, col, GREEN);
      led_matrix__drawPixel(start_row + 2, col, RED);
      led_matrix__drawPixel(start_row + 3, col, RED);
      led_matrix__drawPixel(start_row + 4, col, RED);
      led_matrix__drawPixel(start_row + 5, col, RED);
      led_matrix__drawPixel(start_row + 6, col, RED);
    } else if (col == start_col + 3) {
      led_matrix__drawPixel(start_row + 2, col, RED);
      led_matrix__drawPixel(start_row + 3, col, WHITE);
      led_matrix__drawPixel(start_row + 4, col, RED);
      led_matrix__drawPixel(start_row + 5, col, WHITE);
      led_matrix__drawPixel(start_row + 6, col, RED);
    } else {
      led_matrix__drawPixel(start_row + 3, col, RED);
      led_matrix__drawPixel(start_row + 4, col, RED);
      led_matrix__drawPixel(start_row + 5, col, RED);
    }
  }
}

void draw_end_screen(game_grid_t *gg) {
  led__matrix_clear_screen();

  ////////////////////Display score and start again message/////////////////////// (32, 20)
  int row = 32;
  int col = 8;
  char *score_string = "YOUR SCORE: ";
  draw_string(row, col, score_string, 12, BLUE);

  // for now, have a placeholder.
  row = 40;
  col = 22;
  draw_number(gg->score, row, col, CYAN);

  row = 50;
  col = 12;
  char *press_button_string1 = "PRESS ANY";
  draw_string(row, col, press_button_string1, 9, BLUE);

  row = 56;
  col = 18;
  char *press_button_string2 = "BUTTON";
  draw_string(row, col, press_button_string2, 6, BLUE);

  ////////////////////GAME OVER LETTERS/////////////////////
  // row 6
  row = 6;
  for (int col = 0; col < 64; col++) {
    if ((col >= 20 && col <= 24) || col == 29 || col == 34 || col == 38 || (col >= 41 && col <= 44)) {
      led_matrix__drawPixel(row, col, RED);
    }
  }

  // row 7
  row = 7;
  for (int col = 0; col < 64; col++) {
    if (col == 19 || col == 28 || col == 30 || col == 34 || col == 35 || col == 37 || col == 38 || col == 41) {
      led_matrix__drawPixel(row, col, RED);
    }
  }

  // row 8
  row = 8;
  for (int col = 0; col < 64; col++) {
    if (col == 19 || col == 27 || col == 31 || col == 34 || col == 36 || col == 38 || col == 41) {
      led_matrix__drawPixel(row, col, RED);
    }
  }

  // row 9
  row = 9;
  for (int col = 0; col < 64; col++) {
    if (col == 19 || col == 27 || col == 31 || col == 34 || col == 38 || col == 41) {
      led_matrix__drawPixel(row, col, RED);
    }
  }

  // row 10
  row = 10;
  for (int col = 0; col < 64; col++) {
    if (col == 19 || (col >= 22 && col <= 24) || col == 27 || col == 31 || col == 34 || col == 38 ||
        (col >= 41 && col <= 44)) {
      led_matrix__drawPixel(row, col, RED);
    }
  }

  // row 11
  row = 11;
  for (int col = 0; col < 64; col++) {
    if (col == 19 || col == 22 || col == 24 || (col >= 27 && col <= 31) || col == 34 || col == 38 || col == 41) {
      led_matrix__drawPixel(row, col, RED);
    }
  }

  // row 12
  row = 12;
  for (int col = 0; col < 64; col++) {
    if (col == 19 || col == 24 || col == 27 || col == 31 || col == 34 || col == 38 || col == 41) {
      led_matrix__drawPixel(row, col, RED);
    }
  }

  // row 13
  row = 13;
  for (int col = 0; col < 64; col++) {
    if ((col >= 20 && col <= 24) || col == 27 || col == 31 || col == 34 || col == 38 || (col >= 41 && col <= 44)) {
      led_matrix__drawPixel(row, col, RED);
    }
  }

  // row 16
  row = 16;
  for (int col = 0; col < 64; col++) {
    if ((col >= 20 && col <= 23) || col == 27 || col == 31 || (col >= 34 && col <= 37) || (col >= 41 && col <= 44)) {
      led_matrix__drawPixel(row, col, RED);
    }
  }

  // row 17
  row = 17;
  for (int col = 0; col < 64; col++) {
    if (col == 19 || col == 24 || col == 27 || col == 31 || col == 34 || col == 41 || col == 45) {
      led_matrix__drawPixel(row, col, RED);
    }
  }

  // row 18
  row = 18;
  for (int col = 0; col < 64; col++) {
    if (col == 19 || col == 24 || col == 27 || col == 31 || col == 34 || (col >= 41 && col <= 44)) {
      led_matrix__drawPixel(row, col, RED);
    }
  }

  // row 19
  row = 19;
  for (int col = 0; col < 64; col++) {
    if (col == 19 || col == 24 || col == 27 || col == 31 || col == 34 || col == 41 || col == 45) {
      led_matrix__drawPixel(row, col, RED);
    }
  }

  // row 20
  row = 20;
  for (int col = 0; col < 64; col++) {
    if (col == 19 || col == 24 || col == 27 || col == 31 || (col >= 34 && col <= 37) || col == 41 || col == 45) {
      led_matrix__drawPixel(row, col, RED);
    }
  }

  // row 21
  row = 21;
  for (int col = 0; col < 64; col++) {
    if (col == 19 || col == 24 || col == 27 || col == 31 || col == 34 || col == 41 || col == 45) {
      led_matrix__drawPixel(row, col, RED);
    }
  }

  // row 22
  row = 22;
  for (int col = 0; col < 64; col++) {
    if (col == 19 || col == 24 || col == 28 || col == 30 || col == 34 || col == 41 || col == 45) {
      led_matrix__drawPixel(row, col, RED);
    }
  }

  // row 23
  row = 23;
  for (int col = 0; col < 64; col++) {
    if ((col >= 20 && col <= 23) || col == 29 || (col >= 34 && col <= 37) || col == 41 || col == 45) {
      led_matrix__drawPixel(row, col, RED);
    }
  }
}

void draw_banner(game_grid_t *gg) {
  // a red box enclosing the score/progress bar
  for (int col = 0; col < 64; col++) {
    led_matrix__drawPixel(55, col, WHITE);
    led_matrix__drawPixel(63, col, WHITE);
  }
  for (int row = 55; row < 63; row++) {
    led_matrix__drawPixel(row, 0, WHITE);
    led_matrix__drawPixel(row, 63, WHITE);
  }

  if (current_banner_type == score) {
    // score
    char *score_string = "SCORE: ";
    draw_string(57, 2, score_string, 7, WHITE);
    draw_score(gg);
  } else {
    // progress bar
    draw_progress_bar();
  }
}

void draw_score(game_grid_t *gg) { draw_number(gg->score, 57, 29, CYAN); }

void draw_progress_bar(void) {
  int progress = controller__progress_number_getter();
  for (int col = 1; col < 63; col++) {
    if ((progress / 2) > col) {
      led_matrix__drawPixel(56, col, WHITE);
      led_matrix__drawPixel(57, col, WHITE);
      led_matrix__drawPixel(58, col, WHITE);
      led_matrix__drawPixel(59, col, WHITE);
      led_matrix__drawPixel(60, col, WHITE);
      led_matrix__drawPixel(61, col, WHITE);
      led_matrix__drawPixel(62, col, WHITE);
    } else {
      led_matrix__clearPixel(56, col);
      led_matrix__clearPixel(57, col);
      led_matrix__clearPixel(58, col);
      led_matrix__clearPixel(59, col);
      led_matrix__clearPixel(60, col);
      led_matrix__clearPixel(61, col);
      led_matrix__clearPixel(62, col);
    }
  }
}

void set_current_banner(banner_type banner) {
  if (banner == score)
    current_banner_type = score;
  else
    current_banner_type = progress_bar;
}
