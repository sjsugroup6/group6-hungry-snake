#include <math.h>

#include "controller.h"

// static struct that stores controlelr values
static controller_type controller;
// static shaking progress number
static int shaking_progress_number;

// Pins for the controller
static adc_channel_e joystick_horizontal_adc_channel = ADC__CHANNEL_2;
static adc_channel_e joystick_vertical_adc_channel = ADC__CHANNEL_5;
static port_pin_type yes_button_pin = {0, 6};
static port_pin_type no_button_pin = {0, 7};

// functions used by controller__get_joystick_value
static uint16_t controller__read_horizontal(void);
static uint16_t controller__read_vertical(void);
static bool controller__read_yes_button(void);
static bool controller__read_no_button(void);
static direction_t controller__get_joystick_direction(void);

//////////////////////public functions////////////////////////
void controller__init(void) {

  // init accelerometer
  lsm303__init();

  const uint8_t adc_mode_bit = 1 << 7;
  const uint8_t horizontal_adc_func_bit = 0x1;
  const uint8_t vertical_adc_func_bit = 0x3;

  // init ADC
  LPC_IOCON->P0_25 &= ~(0x7 | adc_mode_bit); // yes button
  LPC_IOCON->P0_25 |= horizontal_adc_func_bit;
  LPC_IOCON->P1_31 &= ~(0x7 | adc_mode_bit); // no button
  LPC_IOCON->P1_31 |= vertical_adc_func_bit;
  adc__initialize();
  adc__enable_burst_mode(ADC__CHANNEL_2);
  adc__enable_more_channel_burst_mode(ADC__CHANNEL_5);

  // init GPIOs (2 buttons )
  LPC_IOCON->P0_6 &= ~0x7; // yes button
  LPC_IOCON->P0_7 &= ~0x7; // no button

  // pull up
  const uint8_t pullup_bit = 1 << 4;
  LPC_IOCON->P0_6 |= pullup_bit;
  LPC_IOCON->P0_7 |= pullup_bit;

  lab_gpio__set_as_input(yes_button_pin.port, yes_button_pin.pin);
  lab_gpio__set_as_input(no_button_pin.port, no_button_pin.pin);
}

// update all fields of the controller struct. should be called in a task
// with a mutex protecting values written/read from the struct
void controller__update(void) {
  controller.current_direction = controller__get_joystick_direction();
  controller.is_yes_button_pressed = controller__read_yes_button();
  controller.is_no_button_pressed = controller__read_no_button();
}

controller_type controller__get_value(void) { return controller; }

////////////////////// shaking-related functions////////////////
void controller__progress_number_setter(float number) {
  int new_value = round(number);
  shaking_progress_number = new_value;
}
int controller__progress_number_getter(void) { return shaking_progress_number; }

//////////////////////static functions////////////////////////
static bool controller__read_yes_button(void) {
  return !(lab_gpio__get_level(yes_button_pin.port, yes_button_pin.pin));
}

static bool controller__read_no_button(void) { return !(lab_gpio__get_level(no_button_pin.port, no_button_pin.pin)); }

static direction_t controller__get_joystick_direction(void) {
  // horizontal testing values: left: x < 60. right: x > 4050
  // vertical testing values: up: y < 80. down: y > 4050
  uint16_t x_value = controller__read_horizontal();
  uint16_t y_value = controller__read_vertical();
  if (x_value < 60 && y_value > 1500) {
    return LEFT;
  } else if (x_value > 4050 && y_value > 1500) {
    return RIGHT;
  } else if (x_value > 1500 && y_value < 80) {
    return UP;
  } else if (x_value > 1500 && y_value > 4050) {
    return DOWN;
  } else {
    return NONE;
  }
}

static uint16_t controller__read_horizontal(void) {
  uint16_t x_value = adc__get_channel_reading_with_burst_mode(joystick_horizontal_adc_channel);
  return x_value;
}

static uint16_t controller__read_vertical(void) {
  uint16_t y_value = adc__get_channel_reading_with_burst_mode(joystick_vertical_adc_channel);
  return y_value;
}