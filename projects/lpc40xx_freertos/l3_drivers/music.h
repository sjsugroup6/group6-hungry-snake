#pragma once

#include "delay.h"
#include "gpio_lab.h"
#include "lpc40xx.h"
#include <stdio.h>

typedef enum { NA = 0, BGM = 1, DEAD = 2, EAT = 3, FASTER = 4 } music_list;

void init_music_pins(void);
void play_bgm_music(void);
void play_faster_music(void);
void play_dead_sound(void);
void play_eat_sound(void);
