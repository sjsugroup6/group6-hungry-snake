#pragma once

#include "LSM303.h"
#include "adc.h"
#include "gpio_lab.h"
#include "lpc40xx.h"
#include "sprite.h" //for direction_t
#include <stdio.h>
#include <string.h>

/*
    setup:
    horizontal joystick pin: ADC__CHANNEL_2 = 2 // P0.25
    vertical joystick pin: ADC__CHANNEL_5 = 5 // P1.31
    yes button pin: p0.6
    no button pin: p0.7
*/

typedef struct {
  direction_t current_direction;
  bool is_yes_button_pressed;
  bool is_no_button_pressed;
} controller_type;

// non-static functions
void controller__init(void);
void controller__update(void);
controller_type controller__get_value(void);
void controller__progress_number_setter(float number);
int controller__progress_number_getter(void);