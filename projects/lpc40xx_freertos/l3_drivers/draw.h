#pragma once

// code used to draw shapes

#include "LED_matrix.h"
#include "controller.h"
#include "lpc40xx.h"
#include "snake/game.h"

typedef enum { score = 0, progress_bar = 1 } banner_type;
typedef enum { difficulty = 0, start = 1 } controller_selection;

///////// basic characters ////////////
int draw_letter(char letter, uint8_t x, uint8_t y, rgb_color_type color);
void draw_number(uint16_t number, uint8_t x, uint8_t y, rgb_color_type color);
void draw_string(uint8_t row, uint8_t col, char *a_string, int string_size, rgb_color_type color);

//////// sprites ///////////
void draw_sprites(sprite_list_t *sprite_list);
void draw_snake(sprite_t *snake);
void draw_obstacle(sprite_t *obstacle);
void draw_fruit(sprite_t *fruit);
void draw_enemy(sprite_t *enemy);

//////// screens /////////
void draw_start_screen(int difficulty, controller_selection selection);
void draw_end_screen(game_grid_t *gg);
void clear_screen(void);
void draw_banner(game_grid_t *gg);
void set_current_banner(banner_type banner); // switch between either progress bar or score
void draw_score(game_grid_t *gg);
void draw_progress_bar(void);
void draw_start_screen_fruit(int start_row, int start_col);