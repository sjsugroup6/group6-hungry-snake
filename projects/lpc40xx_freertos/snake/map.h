#pragma once

#include "game.h"

typedef game_grid_t *(*map_create)(callback, callback, callback, callback, callback);

/**
 * Free the remaining allocated structures that have not been freed yet
 */
void free_map(game_grid_t *gg);

/**
 * Helper function for appending a sprite to the map and properly linking the pointers
 */
void add_sprite_to_map(game_grid_t *map, sprite_t *new_sprite);

/**
 * Generate new fruit at unoccupied position
 */
void generate_new_fruit(game_grid_t *map);

/**
 * Generate new enemy snake at unoccupied position
 */
void generate_new_enemy_snake(game_grid_t *map);

/**
 * Initialize easy map
 */
game_grid_t *init_easy_map(callback normal_bgm, callback faster_bgm, callback eat_sound, callback dead_sound,
                           callback activate_shaking);

/**
 * Initialize medium map by adding on to the easy map
 */
game_grid_t *init_medium_map(callback normal_bgm, callback faster_bgm, callback eat_sound, callback dead_sound,
                             callback activate_shaking);

/**
 * Initialize hard map by adding on to the medium map
 */
game_grid_t *init_hard_map(callback normal_bgm, callback faster_bgm, callback eat_sound, callback dead_sound,
                           callback activate_shaking);

/**
 * Initialize an unrelated frogger map.
 */
game_grid_t *init_frogger_map(callback normal_bgm, callback faster_bgm, callback eat_sound, callback dead_sound,
                              callback activate_shaking);