#include <stdlib.h>

#include "map.h"

void free_map(game_grid_t *gg) {
  sprite_list_t *walker = gg->sprite_list;

  while (walker) {
    sprite_destroy(walker->sprite);
    walker = walker->next;
  }
  free(walker);
}

void add_sprite_to_map(game_grid_t *map, sprite_t *new_sprite) {
  sprite_list_t *prev_list = NULL;
  sprite_list_t *sprite_list_walker = map->sprite_list;
  body_t *sprite_body_walker = new_sprite->head;

  if (!sprite_list_walker) {
    map->sprite_list = calloc(1, sizeof(sprite_list_t));
    map->sprite_list->sprite = new_sprite;
    return;
  }

  while (sprite_list_walker) {
    prev_list = sprite_list_walker;
    sprite_list_walker = sprite_list_walker->next;
  }

  prev_list->next = calloc(1, sizeof(sprite_list_t));
  prev_list->next->prev = prev_list;
  prev_list->next->sprite = new_sprite;

  // Populate the collision grid
  while (sprite_body_walker) {
    map->occupied[sprite_body_walker->x][sprite_body_walker->y] = prev_list->next;
    sprite_body_walker = sprite_body_walker->prev;
  }
}

void generate_new_fruit(game_grid_t *map) {
  int retries = 50;
  int row;
  int col;

  // Find a random spot that is not occupied
  do {
    row = rand() % ROWS;
    col = rand() % COLS;
    retries--;
  } while (map->occupied[row][col] && retries);

  if (map->occupied[row][col] || !retries) {
    return;
  }

  sprite_t *fruit = calloc(1, sizeof(sprite_t));
  // Initialize sprite object
  fruit->type = FRUIT;
  fruit->dir = NONE;
  fruit->speed = 0;
  fruit->length = 1;
  fruit->max_length = 1;

  coordinates_t body_fruit[] = {{row, col}};
  int length = sizeof(body_fruit) / sizeof(coordinates_t);
  sprite_create_linked_bodies(fruit, body_fruit, length);
  add_sprite_to_map(map, fruit);
}

void generate_new_enemy_snake(game_grid_t *map) {
  // Initialize sprite object
  sprite_t *snake = calloc(1, sizeof(sprite_t));
  snake->type = ENEMY;
  snake->speed = 8;
  snake->wrap_around = true;

  // Create a snake of length 3 - 10
  int snake_length = (rand() % 8) + 3;
  snake->length = snake_length;
  snake->max_length = snake_length;

  int retries = 50;
  int row;
  int col;

  // Find a random spot that is not occupied
  do {
    row = rand() % ROWS;
    col = rand() % COLS;
    retries--;
  } while (map->occupied[row][col] && retries);

  if (map->occupied[row][col] || !retries) {
    return;
  }

  coordinates_t *snake_body_parts = calloc(snake_length, sizeof(coordinates_t));
  // Let's put every body part on the same pixel and let the move behavior eventually expand out
  for (int i = 0; i < snake_length; i++) {
    snake_body_parts[i].x = row;
    snake_body_parts[i].y = col;
  }
  sprite_create_linked_bodies(snake, snake_body_parts, snake_length);
  free(snake_body_parts);

  // Generate an advanced movement behavior
  behavior_gen_t snake_behavior_gen_info[] = {
      {TRAIL, true, 10, {RIGHT, RANDOM, RIGHT, RANDOM, RIGHT, RANDOM, RIGHT, RANDOM, RIGHT, RANDOM}},
      {TRAIL, true, 10, {DOWN, RANDOM, DOWN, NONE, DOWN, RANDOM, DOWN, RANDOM, DOWN, RANDOM}},
      {TRAIL, true, 10, {LEFT, RANDOM, LEFT, RANDOM, LEFT, NONE, LEFT, RANDOM, LEFT, NONE}},
      {TRAIL, true, 10, {UP, NONE, UP, RANDOM, UP, RANDOM, UP, RANDOM, UP, RANDOM}},
      {UNIT, true, 10, {RANDOM, RANDOM, RANDOM, RANDOM, RANDOM, RANDOM, RANDOM, RANDOM, RANDOM, RANDOM}}};
  int snake_behavior_length = sizeof(snake_behavior_gen_info) / sizeof(behavior_gen_t);
  sprite_create_move_behaviors(snake, snake_behavior_gen_info, snake_behavior_length);
  add_sprite_to_map(map, snake);
}

game_grid_t *init_easy_map(callback normal_bgm, callback faster_bgm, callback eat_sound, callback dead_sound,
                           callback activate_shaking) {

  // Use calloc so that the structure is zeroed by default. This is primarily
  // important for the body_t structures so each prev/next is NULL by default.
  game_grid_t *map = calloc(1, sizeof(game_grid_t));
  sprite_t *snake = calloc(1, sizeof(sprite_t));

  // Attach callbacks
  map->normal_bgm = normal_bgm;
  map->faster_bgm = faster_bgm;
  map->eat_sound = eat_sound;
  map->dead_sound = dead_sound;
  map->activate_shaking = activate_shaking;

  // Initialize scoring system
  map->multiplier = 1;
  map->score_over_time = true;

  // Initialize sprite object
  snake->type = SNAKE;
  snake->dir = LEFT;
  snake->speed = 5;
  snake->length = 10;
  snake->max_length = 1000;
  snake->wrap_around = true;
  snake->continuous_movement = true;

  // Let's construct a snake occupying the coordinates:
  coordinates_t body_parts[] = {{20, 26}, {20, 27}, {20, 28}, {20, 29}, {20, 30},
                                {20, 31}, {20, 32}, {20, 33}, {20, 34}, {20, 35}};
  int snake_length = sizeof(body_parts) / sizeof(coordinates_t);
  sprite_create_linked_bodies(snake, body_parts, snake_length);
  add_sprite_to_map(map, snake);

  // Top left cross obstacle
  sprite_t *cross_topleft = calloc(1, sizeof(sprite_t));
  // Initialize sprite object
  cross_topleft->type = OBSTACLE;
  cross_topleft->dir = NONE;
  cross_topleft->speed = 0;
  cross_topleft->length = 18;
  cross_topleft->max_length = -1;
  cross_topleft->wrap_around = true;

  // Let's construct an obstacle occupying the coordinates:
  coordinates_t cross_topleft_body_parts[] = {{6, 12},  {7, 12},  {8, 12},  {9, 12},  {10, 12}, {11, 12},
                                              {12, 12}, {13, 12}, {14, 12}, {15, 12}, {10, 8},  {10, 9},
                                              {10, 10}, {10, 11}, {10, 13}, {10, 14}, {10, 15}, {10, 16}};

  int cross_topleft_length = sizeof(cross_topleft_body_parts) / sizeof(coordinates_t);
  sprite_create_linked_bodies(cross_topleft, cross_topleft_body_parts, cross_topleft_length);
  add_sprite_to_map(map, cross_topleft);

  // Top right cross obstacle
  sprite_t *cross_topright = calloc(1, sizeof(sprite_t));
  // Initialize sprite object
  cross_topright->type = OBSTACLE;
  cross_topright->dir = NONE;
  cross_topright->speed = 0;
  cross_topright->length = 18;
  cross_topright->max_length = -1;
  cross_topright->wrap_around = true;

  // Let's construct an obstacle occupying the coordinates:
  coordinates_t cross_topright_body_parts[] = {{6, 46},  {7, 46},  {8, 46},  {9, 46},  {10, 46}, {11, 46},
                                               {12, 46}, {13, 46}, {14, 46}, {15, 46}, {10, 42}, {10, 43},
                                               {10, 44}, {10, 45}, {10, 47}, {10, 48}, {10, 49}, {10, 50}};

  int cross_topright_length = sizeof(cross_topright_body_parts) / sizeof(coordinates_t);
  sprite_create_linked_bodies(cross_topright, cross_topright_body_parts, cross_topright_length);
  add_sprite_to_map(map, cross_topright);

  // Bottom left cross obstacle
  sprite_t *cross_bottomleft = calloc(1, sizeof(sprite_t));
  // Initialize sprite object
  cross_bottomleft->type = OBSTACLE;
  cross_bottomleft->dir = NONE;
  cross_bottomleft->speed = 0;
  cross_bottomleft->length = 18;
  cross_bottomleft->max_length = -1;
  cross_bottomleft->wrap_around = true;

  // Let's construct an obstacle occupying the coordinates:
  coordinates_t cross_bottomleft_body_parts[] = {{39, 12}, {40, 12}, {41, 12}, {42, 12}, {43, 12}, {44, 12},
                                                 {45, 12}, {46, 12}, {47, 12}, {48, 12}, {43, 8},  {43, 9},
                                                 {43, 10}, {43, 11}, {43, 13}, {43, 14}, {43, 15}, {43, 16}};

  int cross_bottomleft_length = sizeof(cross_bottomleft_body_parts) / sizeof(coordinates_t);
  sprite_create_linked_bodies(cross_bottomleft, cross_bottomleft_body_parts, cross_bottomleft_length);
  add_sprite_to_map(map, cross_bottomleft);

  // Bottom right cross obstacle
  sprite_t *cross_bottomright = calloc(1, sizeof(sprite_t));
  // Initialize sprite object
  cross_bottomright->type = OBSTACLE;
  cross_bottomright->dir = NONE;
  cross_bottomright->speed = 0;
  cross_bottomright->length = 18;
  cross_bottomright->max_length = -1;
  cross_bottomright->wrap_around = true;

  // Let's construct an obstacle occupying the coordinates:
  coordinates_t cross_bottomright_body_parts[] = {{39, 46}, {40, 46}, {41, 46}, {42, 46}, {43, 46}, {44, 46},
                                                  {45, 46}, {46, 46}, {47, 46}, {48, 46}, {43, 42}, {43, 43},
                                                  {43, 44}, {43, 45}, {43, 47}, {43, 48}, {43, 49}, {43, 50}};

  int cross_bottomright_length = sizeof(cross_bottomright_body_parts) / sizeof(coordinates_t);
  sprite_create_linked_bodies(cross_bottomright, cross_bottomright_body_parts, cross_bottomright_length);
  add_sprite_to_map(map, cross_bottomright);

  // Create a fruit
  sprite_t *fruit = calloc(1, sizeof(sprite_t));
  // Initialize sprite object
  fruit->type = FRUIT;
  fruit->dir = NONE;
  fruit->speed = 0;
  fruit->length = 1;
  fruit->max_length = 1;

  coordinates_t body_fruit[] = {{10, 29}};
  int fruit_length = sizeof(body_fruit) / sizeof(coordinates_t);
  sprite_create_linked_bodies(fruit, body_fruit, fruit_length);
  add_sprite_to_map(map, fruit);

  return map;
}

game_grid_t *init_medium_map(callback normal_bgm, callback faster_bgm, callback eat_sound, callback dead_sound,
                             callback activate_shaking) {
  game_grid_t *map = init_easy_map(normal_bgm, faster_bgm, eat_sound, dead_sound, activate_shaking);
  sprite_t *sweep_left_wall = calloc(1, sizeof(sprite_t));

  // Initialize sprite object
  sweep_left_wall->type = OBSTACLE;
  sweep_left_wall->speed = 12;
  sweep_left_wall->length = 3;
  sweep_left_wall->max_length = 3;
  sweep_left_wall->wrap_around = true;

  // Let's construct a simple obstacle occupying the coordinates:
  coordinates_t sweep_left_wall_body_parts[] = {{27, 0}, {28, 0}, {29, 0}};
  int sweep_left_wall_body_length = sizeof(sweep_left_wall_body_parts) / sizeof(coordinates_t);
  sprite_create_linked_bodies(sweep_left_wall, sweep_left_wall_body_parts, sweep_left_wall_body_length);

  // Make this obstacle move as a unit RIGHT every step
  behavior_gen_t sweep_left_wall_behavior_gen_info[] = {{UNIT, true, 1, {RIGHT}}};
  int sweep_left_wall_behavior_length = sizeof(sweep_left_wall_behavior_gen_info) / sizeof(behavior_gen_t);
  sprite_create_move_behaviors(sweep_left_wall, sweep_left_wall_behavior_gen_info, sweep_left_wall_behavior_length);
  add_sprite_to_map(map, sweep_left_wall);

  sprite_t *sweep_down_wall = calloc(1, sizeof(sprite_t));

  // Initialize sprite object
  sweep_down_wall->type = OBSTACLE;
  sweep_down_wall->speed = 20;
  sweep_down_wall->length = 3;
  sweep_down_wall->max_length = 3;
  sweep_down_wall->wrap_around = true;

  // Let's construct a simple obstacle occupying the coordinates:
  coordinates_t sweep_down_wall_body_parts[] = {{15, 28}, {15, 29}, {15, 30}};
  int sweep_down_wall_body_length = sizeof(sweep_down_wall_body_parts) / sizeof(coordinates_t);
  sprite_create_linked_bodies(sweep_down_wall, sweep_down_wall_body_parts, sweep_down_wall_body_length);

  // Make this obstacle move as a unit RIGHT every step
  behavior_gen_t sweep_down_wall_behavior_gen_info[] = {{UNIT, true, 1, {DOWN}}};
  int sweep_down_wall_behavior_length = sizeof(sweep_down_wall_behavior_gen_info) / sizeof(behavior_gen_t);
  sprite_create_move_behaviors(sweep_down_wall, sweep_down_wall_behavior_gen_info, sweep_down_wall_behavior_length);
  add_sprite_to_map(map, sweep_down_wall);

  return map;
}

game_grid_t *init_hard_map(callback normal_bgm, callback faster_bgm, callback eat_sound, callback dead_sound,
                           callback activate_shaking) {
  game_grid_t *map = init_medium_map(normal_bgm, faster_bgm, eat_sound, dead_sound, activate_shaking);

  // Initialize sprite object
  sprite_t *snake1 = calloc(1, sizeof(sprite_t));
  snake1->type = ENEMY;
  snake1->speed = 8;
  snake1->length = 5;
  snake1->max_length = 10;
  snake1->wrap_around = true;

  // Let's construct an enemy snake occupying the coordinates:
  coordinates_t snake1_body_parts[] = {{38, 20}, {39, 20}, {40, 20}, {41, 20}, {42, 20}};
  int snake1_body_length = sizeof(snake1_body_parts) / sizeof(coordinates_t);
  sprite_create_linked_bodies(snake1, snake1_body_parts, snake1_body_length);

  // Generate an advanced movement behavior
  behavior_gen_t snake1_behavior_gen_info[] = {
      {TRAIL, true, 5, {RIGHT, RANDOM, RIGHT, RANDOM, RIGHT}},
      {TRAIL, true, 5, {UP, RANDOM, UP, RANDOM, UP}},
      {UNIT, true, 10, {RANDOM, RANDOM, RANDOM, RANDOM, RANDOM, RANDOM, RANDOM, RANDOM, RANDOM, RANDOM}}};
  int snake1_behavior_length = sizeof(snake1_behavior_gen_info) / sizeof(behavior_gen_t);
  sprite_create_move_behaviors(snake1, snake1_behavior_gen_info, snake1_behavior_length);
  add_sprite_to_map(map, snake1);

  // Initialize sprite object
  sprite_t *snake2 = calloc(1, sizeof(sprite_t));
  snake2->type = ENEMY;
  snake2->speed = 8;
  snake2->length = 5;
  snake2->max_length = 10;
  snake2->wrap_around = true;

  // Let's construct an enemy snake occupying the coordinates:
  coordinates_t snake2_body_parts[] = {{26, 44}, {27, 44}, {28, 44}, {29, 44}, {30, 44}};
  int snake2_body_length = sizeof(snake2_body_parts) / sizeof(coordinates_t);
  sprite_create_linked_bodies(snake2, snake2_body_parts, snake2_body_length);

  // Generate an advanced movement behavior
  behavior_gen_t snake2_behavior_gen_info[] = {
      {TRAIL, true, 5, {LEFT, RANDOM, LEFT, RANDOM, LEFT}},
      {TRAIL, true, 5, {DOWN, RANDOM, DOWN, RANDOM, DOWN}},
      {UNIT, true, 10, {RANDOM, RANDOM, RANDOM, RANDOM, RANDOM, RANDOM, RANDOM, RANDOM, RANDOM, RANDOM}}};
  int snake2_behavior_length = sizeof(snake2_behavior_gen_info) / sizeof(behavior_gen_t);
  sprite_create_move_behaviors(snake2, snake2_behavior_gen_info, snake2_behavior_length);
  add_sprite_to_map(map, snake2);

  // Initialize sprite object
  sprite_t *snake3 = calloc(1, sizeof(sprite_t));
  snake3->type = ENEMY;
  snake3->speed = 8;
  snake3->length = 8;
  snake3->max_length = 10;
  snake3->wrap_around = true;

  // Let's construct an enemy snake occupying the coordinates:
  coordinates_t snake3_body_parts[] = {{15, 3}, {14, 3}, {13, 3}, {12, 3}, {11, 3}, {10, 3}, {9, 3}, {8, 3}};
  int snake3_body_length = sizeof(snake3_body_parts) / sizeof(coordinates_t);
  sprite_create_linked_bodies(snake3, snake3_body_parts, snake3_body_length);

  // Generate an advanced movement behavior
  behavior_gen_t snake3_behavior_gen_info[] = {
      {TRAIL, true, 5, {LEFT, LEFT, LEFT, LEFT, LEFT}},
      {TRAIL, true, 5, {DOWN, RANDOM, DOWN, RANDOM, DOWN}},
      {TRAIL, true, 5, {RIGHT, RIGHT, DOWN, RANDOM, DOWN}},
      {UNIT, true, 10, {RANDOM, RANDOM, RANDOM, RANDOM, RANDOM, RANDOM, RANDOM, RANDOM, RANDOM, RANDOM}}};
  int snake3_behavior_length = sizeof(snake3_behavior_gen_info) / sizeof(behavior_gen_t);
  sprite_create_move_behaviors(snake3, snake3_behavior_gen_info, snake3_behavior_length);
  add_sprite_to_map(map, snake3);

  return map;
}

game_grid_t *init_frogger_map(callback normal_bgm, callback faster_bgm, callback eat_sound, callback dead_sound,
                              callback activate_shaking) {
  // Use calloc so that the structure is zeroed by default. This is primarily
  // important for the body_t structures so each prev/next is NULL by default.
  game_grid_t *map = calloc(1, sizeof(game_grid_t));

  // Attach callbacks
  map->normal_bgm = normal_bgm;
  map->faster_bgm = faster_bgm;
  map->eat_sound = eat_sound;
  map->dead_sound = dead_sound;
  map->activate_shaking = activate_shaking;

  // Initialize sprite object
  sprite_t *frog = calloc(1, sizeof(sprite_t));
  frog->type = SNAKE;
  frog->dir = NONE;
  frog->move_type = UNIT;
  frog->speed = 2;
  frog->length = 1;
  frog->max_length = 1;
  frog->wrap_around = true;
  frog->continuous_movement = false;
  frog->dead = false;

  // Let's construct a simple frog
  coordinates_t frog_body_parts[] = {{51, 32}, {52, 31}, {52, 32}, {52, 33}, {53, 31},
                                     {53, 32}, {53, 33}, {54, 31}, {54, 32}, {54, 33}};
  int frog_body_length = sizeof(frog_body_parts) / sizeof(coordinates_t);
  sprite_create_linked_bodies(frog, frog_body_parts, frog_body_length);
  add_sprite_to_map(map, frog);

  sprite_t *cars_right_slow = calloc(1, sizeof(sprite_t));
  cars_right_slow->type = OBSTACLE;
  cars_right_slow->speed = 7;
  cars_right_slow->length = 18;
  cars_right_slow->max_length = -1;
  cars_right_slow->wrap_around = true;

  // Let's construct a row of cars moving right with spaces in between each car
  coordinates_t cars_right_slow_body_parts[] = {
      {10, 0},  {11, 0},  {12, 0},  {13, 0},  {14, 0},  {10, 1},  {11, 1},  {12, 1},  {13, 1},  {14, 1},
      {10, 2},  {11, 2},  {12, 2},  {13, 2},  {14, 2},  {10, 3},  {11, 3},  {12, 3},  {13, 3},  {14, 3},

      {10, 12}, {11, 12}, {12, 12}, {13, 12}, {14, 12}, {10, 13}, {11, 13}, {12, 13}, {13, 13}, {14, 13},
      {10, 14}, {11, 14}, {12, 14}, {13, 14}, {14, 14}, {10, 15}, {11, 15}, {12, 15}, {13, 15}, {14, 15},

      {10, 24}, {11, 24}, {12, 24}, {13, 24}, {14, 24}, {10, 25}, {11, 25}, {12, 25}, {13, 25}, {14, 25},
      {10, 26}, {11, 26}, {12, 26}, {13, 26}, {14, 26}, {10, 27}, {11, 27}, {12, 27}, {13, 27}, {14, 27},

      {10, 36}, {11, 36}, {12, 36}, {13, 36}, {14, 36}, {10, 37}, {11, 37}, {12, 37}, {13, 37}, {14, 37},
      {10, 38}, {11, 38}, {12, 38}, {13, 38}, {14, 38}, {10, 39}, {11, 39}, {12, 39}, {13, 39}, {14, 39},

      {10, 48}, {11, 48}, {12, 48}, {13, 48}, {14, 48}, {10, 49}, {11, 49}, {12, 49}, {13, 49}, {14, 49},
      {10, 50}, {11, 50}, {12, 50}, {13, 50}, {14, 50}, {10, 51}, {11, 51}, {12, 51}, {13, 51}, {14, 51}};

  int cars_right_slow_body_length = sizeof(cars_right_slow_body_parts) / sizeof(coordinates_t);
  sprite_create_linked_bodies(cars_right_slow, cars_right_slow_body_parts, cars_right_slow_body_length);

  // Have cars only move right
  behavior_gen_t cars_right_slow_behavior_gen_info[] = {{UNIT, true, 1, {RIGHT}}};
  int cars_right_slow_behavior_length = sizeof(cars_right_slow_behavior_gen_info) / sizeof(behavior_gen_t);
  sprite_create_move_behaviors(cars_right_slow, cars_right_slow_behavior_gen_info, cars_right_slow_behavior_length);
  add_sprite_to_map(map, cars_right_slow);

  // FAST LEFT CARS
  sprite_t *cars_left_fast = calloc(1, sizeof(sprite_t));
  cars_left_fast->type = OBSTACLE;
  cars_left_fast->speed = 5;
  cars_left_fast->length = 18;
  cars_left_fast->max_length = -1;
  cars_left_fast->wrap_around = true;

  // Let's construct a row of cars moving left with spaces in between each car
  // We will use the previous set of coordinates but shift the cars downward
  coordinates_t cars_left_fast_body_parts[cars_right_slow_body_length];
  for (int i = 0; i < cars_right_slow_body_length; i++) {
    cars_left_fast_body_parts[i].x = cars_right_slow_body_parts[i].x + 12;
    cars_left_fast_body_parts[i].y = cars_right_slow_body_parts[i].y;
  }

  int cars_left_fast_body_length = sizeof(cars_left_fast_body_parts) / sizeof(coordinates_t);
  sprite_create_linked_bodies(cars_left_fast, cars_left_fast_body_parts, cars_left_fast_body_length);

  // Have cars only move left
  behavior_gen_t cars_left_fast_behavior_gen_info[] = {{UNIT, true, 1, {LEFT}}};
  int cars_left_fast_behavior_length = sizeof(cars_left_fast_behavior_gen_info) / sizeof(behavior_gen_t);
  sprite_create_move_behaviors(cars_left_fast, cars_left_fast_behavior_gen_info, cars_left_fast_behavior_length);
  add_sprite_to_map(map, cars_left_fast);

  // FAST RIGHT CARS
  sprite_t *cars_right_fast = calloc(1, sizeof(sprite_t));
  cars_right_fast->type = OBSTACLE;
  cars_right_fast->speed = 4;
  cars_right_fast->length = 18;
  cars_right_fast->max_length = -1;
  cars_right_fast->wrap_around = true;

  // Let's construct a row of cars moving left with spaces in between each car
  // We will use the previous set of coordinates but shift the cars downward
  coordinates_t cars_right_fast_body_parts[cars_right_slow_body_length];
  for (int i = 0; i < cars_right_slow_body_length; i++) {
    cars_right_fast_body_parts[i].x = cars_right_slow_body_parts[i].x + 24;
    cars_right_fast_body_parts[i].y = cars_right_slow_body_parts[i].y;
  }

  int cars_right_fast_body_length = sizeof(cars_right_fast_body_parts) / sizeof(coordinates_t);
  sprite_create_linked_bodies(cars_right_fast, cars_right_fast_body_parts, cars_right_fast_body_length);

  // Have cars only move left
  behavior_gen_t cars_right_fast_behavior_gen_info[] = {{UNIT, true, 1, {RIGHT}}};
  int cars_right_fast_behavior_length = sizeof(cars_right_fast_behavior_gen_info) / sizeof(behavior_gen_t);
  sprite_create_move_behaviors(cars_right_fast, cars_right_fast_behavior_gen_info, cars_right_fast_behavior_length);
  add_sprite_to_map(map, cars_right_fast);

  // SLOW LEFT CARS
  sprite_t *cars_left_slow = calloc(1, sizeof(sprite_t));
  cars_left_slow->type = OBSTACLE;
  cars_left_slow->speed = 8;
  cars_left_slow->length = 18;
  cars_left_slow->max_length = -1;
  cars_left_slow->wrap_around = true;

  // Let's construct a row of cars moving left with spaces in between each car
  // We will use the previous set of coordinates but shift the cars downward
  coordinates_t cars_left_slow_body_parts[cars_right_slow_body_length];
  for (int i = 0; i < cars_right_slow_body_length; i++) {
    cars_left_slow_body_parts[i].x = cars_right_slow_body_parts[i].x + 36;
    cars_left_slow_body_parts[i].y = cars_right_slow_body_parts[i].y;
  }

  int cars_left_slow_body_length = sizeof(cars_left_slow_body_parts) / sizeof(coordinates_t);
  sprite_create_linked_bodies(cars_left_slow, cars_left_slow_body_parts, cars_left_slow_body_length);

  // Have cars only move left
  behavior_gen_t cars_left_slow_behavior_gen_info[] = {{UNIT, true, 1, {LEFT}}};
  int cars_left_slow_behavior_length = sizeof(cars_left_slow_behavior_gen_info) / sizeof(behavior_gen_t);
  sprite_create_move_behaviors(cars_left_slow, cars_left_slow_behavior_gen_info, cars_left_slow_behavior_length);
  add_sprite_to_map(map, cars_left_slow);

  // Generate a fruit at a random unoccupied location
  generate_new_fruit(map);

  return map;
}