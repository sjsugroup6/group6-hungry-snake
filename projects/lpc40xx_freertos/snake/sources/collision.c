#include "stdio.h"

#include "collision.h"
#include "map.h"

collision_status_t sprite_collision(game_grid_t *gg, int x, int y, sprite_t *old_sprite, sprite_t *new_sprite) {
  // The snake has hit his own body and should die
  if (old_sprite == new_sprite && new_sprite->type == SNAKE) {
    if (gg->dead_sound) {
      gg->dead_sound();
    }
    old_sprite->dead = true;
    // Don't return BOTH_DIES here because we don't want the caller to double free the same sprite
    return NEW_DIES;
  }

  // If neither sprite is the player (SNAKE), then let's not "kill" them.
  // TODO: We need to modify this condition to handle consumable FRUIT types
  if (old_sprite->type != SNAKE && new_sprite->type != SNAKE) {
    return NO_ACTION_NEW_DOMINANT;
  }

  // Handle snake + fruit collision
  if (old_sprite->type == FRUIT && new_sprite->type == SNAKE) {
    if (gg->eat_sound) {
      gg->eat_sound();
    }
    old_sprite->dead = true;
    gg->multiplier++;
    gg->score += 100;
    sprite_prepare_extension_for_next_movement(new_sprite);
    generate_new_fruit(gg);
    return OLD_DIES;
  }
  if (old_sprite->type == SNAKE && new_sprite->type == FRUIT) {
    if (gg->eat_sound) {
      gg->eat_sound();
    }
    new_sprite->dead = true;
    gg->multiplier++;
    gg->score += 100;
    sprite_prepare_extension_for_next_movement(old_sprite);
    generate_new_fruit(gg);
    return NEW_DIES;
  }

  // Handle snake + obstacle collision
  if (old_sprite->type == SNAKE && new_sprite->type == OBSTACLE) {
    if (gg->dead_sound) {
      gg->dead_sound();
    }
    old_sprite->dead = true;
    return OLD_DIES;
  }
  if (old_sprite->type == OBSTACLE && new_sprite->type == SNAKE) {
    if (gg->dead_sound) {
      gg->dead_sound();
    }
    new_sprite->dead = true;
    return NEW_DIES;
  }

  // TODO: Prevent game step from happening after shaking has been activated
  // Handle snake + enemy_snake collision
  if (old_sprite->type == SNAKE && new_sprite->type == ENEMY) {
    pause_game(gg);
    if (gg->faster_bgm) {
      gg->faster_bgm();
    }
    gg->fighting_snake = new_sprite;
    if (gg->activate_shaking) {
      gg->activate_shaking();
    }
  }
  if (old_sprite->type == ENEMY && new_sprite->type == SNAKE) {
    pause_game(gg);
    if (gg->faster_bgm) {
      gg->faster_bgm();
    }
    gg->fighting_snake = old_sprite;
    if (gg->activate_shaking) {
      gg->activate_shaking();
    }
  }

  // TODO: We are temporarily returning anything to prevent compiler warnings
  return NO_ACTION_OLD_DOMINANT;
}