#include <assert.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

#include "game.h"
#include "sprite.h"

void sprite_step(sprite_t *sprite) {
  if (sprite->type != SNAKE && sprite->speed == 0) {
    return;
  }

  // This technique is used simluate different speed sprites. A higher speed value means the sprite is slower.
  sprite->tick += 1;
  if (sprite->tick < sprite->speed) {
    return;
  }
  sprite->tick = 0;

  sprite_handle_pending_actions(sprite);

  // If the sprite is the player and the speed is 0, that means no automatic movement will happen.
  // However, the player should be able to still move its own sprite one unit if the player has
  // a direction held down.
  if (sprite->type == SNAKE) {
    if (sprite->move_type == TRAIL) {
      move_as_trail(sprite);
      if (!sprite->continuous_movement) {
        sprite->dir = NONE;
      }
    } else if (sprite->move_type == UNIT) {
      move_as_unit(sprite);
      if (!sprite->continuous_movement) {
        sprite->dir = NONE;
      }
    }
    return;
  }

  // If we reach here, we are handling NPC movement
  if (!sprite->move_behavior) {
    return;
  }

  if (sprite->move_behavior->sequence_complete) {
    // TODO: Randomize next movement behavior
    sprite->move_behavior = sprite->move_behavior->prev;
    sprite->move_behavior->sequence_complete = false;
    sprite->move_behavior->last_dir = NULL;
  }

  switch (sprite->move_behavior->move_type) {
  case TRAIL:
    move_as_trail(sprite);
    break;
  case UNIT:
    move_as_unit(sprite);
  }
}

void move_as_unit(sprite_t *sprite) {
  body_t *walker = sprite->head;

  if (sprite->type != SNAKE) {
    if (sprite->move_behavior->last_dir) {
      sprite->move_behavior->last_dir = sprite->move_behavior->last_dir->prev;
    } else {
      sprite->move_behavior->last_dir = sprite->move_behavior->dir_list_head;
    }
    if (sprite->move_behavior->last_dir == sprite->move_behavior->dir_list_tail) {
      sprite->move_behavior->sequence_complete = true;
    }
    sprite->dir = sprite->move_behavior->last_dir->direction;
  }

  if (sprite->dir == RANDOM) {
    switch (rand() % 4) {
    case 0:
      sprite->dir = UP;
      break;
    case 1:
      sprite->dir = DOWN;
      break;
    case 2:
      sprite->dir = LEFT;
      break;
    case 3:
      sprite->dir = RIGHT;
    }
  }

  while (walker) {
    switch (sprite->dir) {
    case UP:
      walker->x--;
      break;
    case DOWN:
      walker->x++;
      break;
    case LEFT:
      walker->y--;
      break;
    case RIGHT:
      walker->y++;
      break;
    case RANDOM:
      assert(0);
    case NONE:
      // Our game module will eventually call this function with all sprites.
      // In case of non-moving obstacles or fruits, we want to do nothing.
      return;
    }

    // Adjust the coordinates if this sprite has wrap_around enabled
    if (sprite->wrap_around) {
      walker->x %= ROWS;
      walker->y %= COLS;
    }

    // Fix the new coordinates if they are negative
    if (walker->x < 0) {
      walker->x = ROWS - 1;
    }
    if (walker->y < 0) {
      walker->y = COLS - 1;
    }

    // Process the next body part
    walker = walker->prev;
  }
}

void move_as_trail(sprite_t *sprite) {
  /**
   * Assuming a snake of length 3, here are the minimalistic representations
   * for each group of bodies that are adjacent to each other in one direction.
   * (5,5) will be the head at T=0 in all the examples.
   *
   * Coordinate System
   *
   *  (0,0)   (0,1)  (0,2)  ..  (0,63)
   *  (1,0)    ..     ..    ..    ..
   *   ..      ..     ..    ..    ..
   *  (63,0)  (0,1)  (0,2)  ..  (63,63)
   *
   * Leftward + 0
   *             <- (5,5) <- (5,6) <- (5,7  )
   * Leftward + 1
   *    <- (5,4) <- (5,5) <- (5,6)
   *
   * Rightward + 0
   *    (5,3) -> (5,4) -> (5,5) ->
   * Rightward + 1
   *             (5,4) -> (5,5) -> (5,6) ->
   *
   * Upward + 0     Upward + 1
   *                        ^
   *                      (2,5)
   *         ^              ^
   *       (3,5)          (3,5)
   *         ^              ^
   *       (4,5)          (4,5)
   *         ^
   *       (5,5)
   *
   * Downard + 0    Downward + 1
   *       (5,5)
   *         v
   *       (6,5)          (6,5)
   *         v              v
   *       (7,5)          (7,5)
   *         v              v
   *                      (8,5)
   *                        v
   */

  int new_head_x = sprite->head->x;
  int new_head_y = sprite->head->y;
  body_t *tail = sprite->tail;
  body_t *front = tail->next;

  if (sprite->type != SNAKE) {
    if (sprite->move_behavior->last_dir) {
      sprite->move_behavior->last_dir = sprite->move_behavior->last_dir->prev;
    } else {
      sprite->move_behavior->last_dir = sprite->move_behavior->dir_list_head;
    }
    if (sprite->move_behavior->last_dir == sprite->move_behavior->dir_list_tail) {
      sprite->move_behavior->sequence_complete = true;
    }
    sprite->dir = sprite->move_behavior->last_dir->direction;
  }

  if (sprite->dir == RANDOM) {
    switch (rand() % 4) {
    case 0:
      sprite->dir = UP;
      break;
    case 1:
      sprite->dir = DOWN;
      break;
    case 2:
      sprite->dir = LEFT;
      break;
    case 3:
      sprite->dir = RIGHT;
    }
  }

  switch (sprite->dir) {
  case UP:
    new_head_x--;
    break;
  case DOWN:
    new_head_x++;
    break;
  case LEFT:
    new_head_y--;
    break;
  case RIGHT:
    new_head_y++;
    break;
  case RANDOM:
    assert(0);
  case NONE:
    // Our game module will eventually call this function with all sprites.
    // In case of non-moving obstacles or fruits, we want to do nothing.
    return;
  }

  // Walk from the tail to the head and fill in the tail's coordinates with its front neighbor
  while (front) {
    if (tail->new_extension) {
      // When we call sprite_extend(), the new body will have the same coordinates
      // as the tail. After one tick, all body components will be 1 away from each other.
      tail->new_extension = false;
    } else {
      tail->x = front->x;
      tail->y = front->y;
    }
    tail = front;
    front = front->next;
  }

  // If a sprite that is a snake and is out of bounds let's consider it dead
  if (!sprite->wrap_around && sprite->type == SNAKE &&
      (new_head_x < 0 || new_head_x > ROWS - 1 || new_head_y < 0 || new_head_y > COLS - 1)) {
    sprite->dead = true;
  }

  // Ensure that if you go past the edge that you appear on the opposite edge
  if (sprite->wrap_around) {
    new_head_x %= ROWS;
    new_head_y %= COLS;
  }

  // Fix the new coordinates if they are negative
  if (new_head_x < 0) {
    new_head_x = ROWS - 1;
  }
  if (new_head_y < 0) {
    new_head_y = COLS - 1;
  }

  // Finally update the coordinates of the head
  sprite->head->x = new_head_x;
  sprite->head->y = new_head_y;
}

void sprite_destroy(sprite_t *sprite) {
  body_t *walker = sprite->head;
  body_t *prev;

  while (walker) {
    prev = walker->prev;
    sprite_body_destroy(walker);
    walker = prev;
  }

  sprite_move_behavior_destroy(sprite->move_behavior);
  free(sprite);
}

void sprite_body_destroy(body_t *body) { free(body); }

void sprite_move_behavior_destroy(move_behavior_t *move_behavior) {
  move_behavior_t *move_walker = move_behavior;
  move_behavior_t *move_next = NULL;

  while (move_next != move_behavior) {
    move_next = move_walker->next;

    direction_list_t *dir_walker = move_walker->dir_list_head;
    direction_list_t *dir_next = NULL;
    while (dir_next != move_walker->dir_list_head) {
      dir_next = dir_walker->next;
      free(dir_walker);
      dir_walker = dir_next;
    }

    free(move_behavior);
    move_walker = move_next;
  }
}

bool sprite_extend(sprite_t *sprite) {
  body_t *cur_tail = sprite->tail;
  body_t *new_tail = calloc(1, sizeof(body_t));

  if (sprite->length >= sprite->max_length) {
    return false;
  }

  // The new tail should have the same exact coordinates as the existing tail.
  // The only difference is that we set `new_extension = true`. This will be
  // used in the `sprite_step()` function to make sure that this tail will
  // not move during the next step.
  new_tail->x = cur_tail->x;
  new_tail->y = cur_tail->y;
  new_tail->new_extension = true;
  cur_tail->prev = new_tail;
  new_tail->next = cur_tail;
  sprite->tail = new_tail;
  sprite->length++;
  return true;
}

bool sprite_shrink(sprite_t *sprite) {
  body_t *cur_tail = sprite->tail;
  body_t *new_tail = cur_tail->next;

  if (!new_tail) {
    return false; // Cannot shrink less than 1 unit
  }

  // We will shrink from the tail so adjust the linked list such that the existing tail is removed.
  new_tail->prev = NULL;
  sprite->tail = new_tail;
  if (!new_tail->next) {
    // After shrinking, the length is exactly 1 so the head and tail is the same body part
    sprite->head = new_tail;
  }
  sprite_body_destroy(cur_tail);
  sprite->length--;
  return true;
}

bool sprite_direction_set(sprite_t *sprite, direction_t dir) {
  // Disallow setting a direction opposite of what it was previously moving
  if (sprite->continuous_movement && sprite->speed &&
      ((sprite->dir == UP && dir == DOWN) || (sprite->dir == DOWN && dir == UP) ||
       (sprite->dir == LEFT && dir == RIGHT) || (sprite->dir == RIGHT && dir == LEFT))) {
    return false;
  }
  sprite->dir = dir;
  return true;
}

// TODO: We probably need a sprite_prepare_speed_change_for_next_movement() function
void sprite_speed_inc(sprite_t *sprite) { sprite->speed++; }

void sprite_speed_dec(sprite_t *sprite) { sprite->speed--; }

void sprite_prepare_extension_for_next_movement(sprite_t *sprite) { sprite->pending_action.extend = true; }

void sprite_prepare_shrink_for_next_movement(sprite_t *sprite) { sprite->pending_action.shrink = true; }

void sprite_handle_pending_actions(sprite_t *sprite) {
  // When it's time for the sprite to move again and we detect that we have both an outstanding
  // extend and shrink operation, just cancel them out.
  if (sprite->pending_action.extend && sprite->pending_action.shrink) {
    sprite->pending_action.extend = false;
    sprite->pending_action.shrink = false;
  }

  if (sprite->pending_action.extend) {
    fprintf(stderr, "Extending\n");
    sprite_extend(sprite);
    sprite->pending_action.extend = false;
  }
  if (sprite->pending_action.shrink) {
    sprite_shrink(sprite);
    sprite->pending_action.shrink = false;
  }
}

void sprite_create_linked_bodies(sprite_t *sprite, coordinates_t *coord, int length) {
  body_t *head = calloc(1, sizeof(body_t));
  body_t *walker = head;

  head->x = coord[0].x;
  head->y = coord[0].y;

  for (int i = 1; i < length; i++) {
    body_t *new_body = calloc(1, sizeof(body_t));

    new_body->x = coord[i].x;
    new_body->y = coord[i].y;
    walker->prev = new_body;
    new_body->next = walker;
    walker = new_body;
  }

  sprite->head = head;
  sprite->tail = walker;
}

void sprite_create_move_behaviors(sprite_t *sprite, behavior_gen_t *behavior_gen, int length) {
  move_behavior_t *next = calloc(1, sizeof(move_behavior_t));
  move_behavior_t *prev = next;

  sprite->move_behavior = next;
  next->move_type = behavior_gen[0].move_type;
  next->strict = behavior_gen[0].strict;
  sprite_create_behavior_dir_list(next, behavior_gen[0].directions, behavior_gen[0].direction_count);

  for (int i = 1; i < length; i++) {
    prev = calloc(1, sizeof(move_behavior_t));
    next->prev = prev;
    prev->next = next;
    prev->move_type = behavior_gen[i].move_type;
    prev->strict = behavior_gen[i].strict;
    sprite_create_behavior_dir_list(prev, behavior_gen[i].directions, behavior_gen[i].direction_count);
    next = prev;
  }
  // Finish up the doubly circular linked list
  sprite->move_behavior->next = prev;
  prev->prev = sprite->move_behavior;
}

void sprite_create_behavior_dir_list(move_behavior_t *move_behavior, direction_t *directions, int length) {
  direction_list_t *next = calloc(1, sizeof(direction_list_t));
  direction_list_t *prev = next;

  move_behavior->dir_list_head = next;
  next->direction = directions[0];

  for (int i = 1; i < length; i++) {
    prev = calloc(1, sizeof(direction_list_t));
    next->prev = prev;
    prev->next = next;
    prev->direction = directions[i];
    next = prev;
  }
  // Finish up the doubly circular linked list
  move_behavior->dir_list_head->next = prev;
  move_behavior->dir_list_tail = prev;
  prev->prev = move_behavior->dir_list_head;
}