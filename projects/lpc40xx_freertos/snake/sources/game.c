#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "collision.h"
#include "game.h"
#include "sprite.h"

bool game_step(game_grid_t *gg) {
  static uint32_t step = 0;
  sprite_list_t *walker = gg->sprite_list;

  if (gg->paused) {
    return false;
  }
  if (gg->gameover) {
    return gg->gameover;
  }

  // For each sprite in the grid, perform a step so that the body positions are updated
  // if it is supposed to be moving in a direction.
  while (walker) {
    sprite_step(walker->sprite);
    walker = walker->next;
  }

  // Since the above steps might have changed the positions of some sprites, we need to
  // repopulated the occupied matrix.
  update_occupied_grid(gg);

  if (gg->score_over_time) {
    gg->score += gg->multiplier;
  }
  step++;
  if (gg->gameover && gg->dead_sound) {
    gg->dead_sound();
  }
  return gg->gameover;
}

void pause_game(game_grid_t *gg) { gg->paused = true; }

void resume_game(game_grid_t *gg) { gg->paused = false; }

void set_gameover(game_grid_t *gg) {
  gg->gameover = true;
  resume_game(gg);
}

void win_battle(game_grid_t *gg) {
  if (gg->normal_bgm) {
    gg->normal_bgm();
  }
  gg->fighting_snake->dead = true;
  gg->multiplier++;
  gg->score += 1000;
  generate_new_enemy_snake(gg);
  sprite_prepare_extension_for_next_movement(gg->sprite_list->sprite);
  resume_game(gg);
}

sprite_list_t *remove_sprite_list_elem_from_game(game_grid_t *gg, sprite_list_t *sprite_elem) {
  sprite_list_t *next_sprite_list_elem = sprite_elem->next;
  sprite_t *sprite = sprite_elem->sprite;
  body_t *body_walker = sprite->head;

  // Since we are going to destroy this sprite, let's make sure the grid is cleaned up too
  while (body_walker) {
    gg->occupied[body_walker->x][body_walker->y] = NULL;
    body_walker = body_walker->prev;
  }

  // If the sprite we are about to destroy is a SNAKE, we should mark the game state as gameover
  if (sprite->type == SNAKE) {
    gg->gameover = true;
  }
  // If the sprite we are about to destroy is an ENEMY, clear the reference to the fighting snake
  if (sprite->type == ENEMY) {
    gg->fighting_snake = NULL;
  }
  sprite_destroy(sprite);

  // Remove the dead sprite from the linked list
  if (sprite_elem->prev) {
    sprite_elem->prev->next = sprite_elem->next;
  } else {
    // If there is no previous element, then the next element must be the start of the list
    gg->sprite_list = sprite_elem->next;
  }
  if (sprite_elem->next) {
    sprite_elem->next->prev = sprite_elem->prev;
  }

  free(sprite_elem);
  return next_sprite_list_elem;
}

void reset_occupied_grid(game_grid_t *gg) {
  // Remove occupied spaces on the grid by zeroing grid
  memset(gg, 0, ROWS * COLS * sizeof(sprite_list_t *));
}

void update_occupied_grid(game_grid_t *gg) {
  sprite_list_t *sprite_list_walker = gg->sprite_list;
  sprite_list_t *occupying_sprite_list_elem;
  sprite_t *sprite;
  sprite_t *occupying_sprite;
  body_t *body_walker;
  collision_status_t collision_status;
  uint8_t x;
  uint8_t y;

  reset_occupied_grid(gg);

  // Repopulate the grid using the existing sprites
  while (sprite_list_walker) {
    sprite = sprite_list_walker->sprite;

    // If the sprite has been marked dead, let's free its memory and not consider as
    // part of the map.
    if (sprite->dead) {
      sprite_list_walker = remove_sprite_list_elem_from_game(gg, sprite_list_walker);
      continue;
    }

    // Walk each body part of the sprite and mark the grid as occupied
    body_walker = sprite->head;
    while (body_walker) {
      x = body_walker->x;
      y = body_walker->y;
      occupying_sprite_list_elem = gg->occupied[x][y];

      if (occupying_sprite_list_elem && !body_walker->new_extension) {
        // We have a collision so we need to handle it
        occupying_sprite = occupying_sprite_list_elem->sprite;
        collision_status = sprite_collision(gg, x, y, occupying_sprite, sprite);

        switch (collision_status) {
        case NO_ACTION_OLD_DOMINANT:
          body_walker = body_walker->prev;
          break;
        case NO_ACTION_NEW_DOMINANT:
          gg->occupied[x][y] = sprite_list_walker;
          body_walker = body_walker->prev;
          break;
        case OLD_DIES:
          sprite_list_walker = remove_sprite_list_elem_from_game(gg, occupying_sprite_list_elem);
          gg->occupied[x][y] = sprite_list_walker;
          body_walker = body_walker->prev;
          break;
        case NEW_DIES:
          sprite_list_walker = remove_sprite_list_elem_from_game(gg, sprite_list_walker);
          body_walker = NULL;
          break;
        case BOTH_DIES:
          assert(sprite_list_walker != occupying_sprite_list_elem);
          sprite_list_walker = remove_sprite_list_elem_from_game(gg, occupying_sprite_list_elem);
          sprite_list_walker = remove_sprite_list_elem_from_game(gg, sprite_list_walker);
          body_walker = NULL;
        }

        continue;
      }

      gg->occupied[x][y] = sprite_list_walker;
      body_walker = body_walker->prev;
    }

    // Set walker to the next element in the list
    if (sprite_list_walker) {
      sprite_list_walker = sprite_list_walker->next;
    }
  }
}

void print_game_state(game_grid_t *gg) {
  printf("\n  ");
  for (int i = 0; i < COLS; i++) {
    printf("%02d", i);
  }

  printf("\n");
  for (int i = 0; i < ROWS; i++) {
    printf("%02d ", i);
    for (int j = 0; j < COLS; j++) {
      if (gg->occupied[i][j]) {
        switch (gg->occupied[i][j]->sprite->dir) {
        case NONE:
          printf("X ");
          break;
        case UP:
          printf("^ ");
          break;
        case DOWN:
          printf("v ");
          break;
        case LEFT:
          printf("< ");
          break;
        case RIGHT:
          printf("> ");
        }
      } else {
        printf(". ");
      }
    }
    printf("\n");
  }
}
