#pragma once

#include "sprite.h"

#define ROWS 55
#define COLS 64

typedef void (*callback)(void);

typedef struct SPRITE_LIST {
  sprite_t *sprite;
  struct SPRITE_LIST *next;
  struct SPRITE_LIST *prev;
} sprite_list_t;

typedef struct {
  sprite_list_t *occupied[ROWS][COLS];
  sprite_list_t *sprite_list;
  sprite_t *fighting_snake;
  uint8_t multiplier;
  uint32_t score;
  bool score_over_time;
  bool paused;
  bool gameover;
  callback normal_bgm;
  callback faster_bgm;
  callback eat_sound;
  callback dead_sound;
  callback activate_shaking;
} game_grid_t;

/**
 * Call this function to progress the game state by one step
 */
bool game_step(game_grid_t *gg);

/**
 * Pause game
 */
void pause_game(game_grid_t *gg);

/**
 * Resume game
 */
void resume_game(game_grid_t *gg);

/**
 * Cause the game to end
 */
void set_gameover(game_grid_t *gg);

/**
 * Shaking battle won
 */
void win_battle(game_grid_t *gg);

/**
 * Remove a sprite from the game grid
 */
sprite_list_t *remove_sprite_list_elem_from_game(game_grid_t *gg, sprite_list_t *sprite_elem);

/**
 * Set the entire grid to NULL
 */
void reset_occupied_grid(game_grid_t *gg);

/**
 * After calling `game_step`, we need to update our local occupied grid
 * and check to see if there is a collision
 */
void update_occupied_grid(game_grid_t *gg);

/**
 * Debug function for printing the game state in the terminal
 */
void print_game_state(game_grid_t *gg);