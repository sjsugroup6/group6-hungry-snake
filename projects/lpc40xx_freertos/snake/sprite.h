#pragma once

#include <stdbool.h>
#include <stdint.h>

#define MAX_MOVE_BEHAVIOR 20

typedef enum {
  SNAKE,
  ENEMY,
  FRUIT,
  OBSTACLE,
} sprite_type_t;

typedef enum {
  NONE,
  UP,
  DOWN,
  LEFT,
  RIGHT,
  RANDOM,
} direction_t;

typedef struct DIRECTION_LIST {
  direction_t direction;
  struct DIRECTION_LIST *next;
  struct DIRECTION_LIST *prev;
} direction_list_t;

typedef enum {
  TRAIL, // snake-like movement
  UNIT,  // move as unit
} move_type_t;

typedef struct MOVE_BEHAVIOR {
  move_type_t move_type;
  bool sequence_complete; // If this is true on next movement tick, we can reset this to false go to prev or next
  bool strict; // We are allowed to mark this sequence complete early before going through the entire direction_list
  direction_list_t *dir_list_head;
  direction_list_t *dir_list_tail;
  direction_list_t *last_dir;
  struct MOVE_BEHAVIOR *next;
  struct MOVE_BEHAVIOR *prev;
} move_behavior_t;

typedef struct {
  move_type_t move_type;
  bool strict;
  int direction_count;
  direction_t directions[MAX_MOVE_BEHAVIOR];
} behavior_gen_t;

typedef struct {
  int x;
  int y;
} coordinates_t;

typedef struct {
  bool extend;
  bool shrink;
} pending_action_t;

typedef struct SPRITE_BODY {
  int x;
  int y;
  struct SPRITE_BODY *next;
  struct SPRITE_BODY *prev;
  bool new_extension;
} body_t;

typedef struct {
  sprite_type_t type;
  body_t *head;
  body_t *tail;
  // Used by player controlled sprite and the initial movement direction for non-player controlled sprites
  direction_t dir;
  // Used by player controlled sprite to determine movement type. Movement types for NPCs are defined in `move_behavior`
  move_type_t move_type;
  // Movement behavior for non-player controlled sprites. This is a circular doubly linked list.
  move_behavior_t *move_behavior;
  // Higher value means slower speed
  uint8_t speed;
  // The sprite will only move if tick = speed
  uint8_t tick;
  uint16_t length;
  uint16_t max_length;
  pending_action_t pending_action;
  // Can the body go from one edge and appear on the opposite edge
  bool wrap_around;
  // Will the player sprite move continuosly in their current direction?
  bool continuous_movement;
  bool dead;
} sprite_t;

/**
 * Update the positions of each body part on each tick
 */
void sprite_step(sprite_t *sprite);

/**
 * Movement function where each body part moves in the same direction
 */
void move_as_unit(sprite_t *sprite);

/**
 * Movement function where each body part follows its front neighbor
 */
void move_as_trail(sprite_t *sprite);

/**
 * Destroy the sprite
 */
void sprite_destroy(sprite_t *sprite);

/**
 * Destroy a body component of the sprite
 */
void sprite_body_destroy(body_t *body);

/**
 * Destroy the components of a sprite's movement behavior
 */
void sprite_move_behavior_destroy(move_behavior_t *move_behavior);

/**
 * Extend the tail of the sprite
 */
bool sprite_extend(sprite_t *sprite);

/**
 * Chop off the tail of the sprite
 */
bool sprite_shrink(sprite_t *sprite);

/**
 * Change direction the sprite will take on the next tick
 */
bool sprite_direction_set(sprite_t *sprite, direction_t dir);

/**
 * Increase speed of sprite
 */
void sprite_speed_inc(sprite_t *sprite);

/**
 * Decrease speed of sprite
 */
void sprite_speed_dec(sprite_t *sprite);

/**
 * Prepare for sprite extension on a future tick where the sprite will perform a movement
 */
void sprite_prepare_extension_for_next_movement(sprite_t *sprite);

/**
 * Prepare for a sprite shrink on a future tick where the sprite will perform a movement
 */
void sprite_prepare_shrink_for_next_movement(sprite_t *sprite);

/**
 * Handle all pending actions on next movement
 */
void sprite_handle_pending_actions(sprite_t *sprite);

/**
 * Utility function for automatically generating linked bodies for map generation
 */
void sprite_create_linked_bodies(sprite_t *sprite, coordinates_t *coord, int length);

/*
 * Utility function used to generate advanced movement behaviors
 */
void sprite_create_move_behaviors(sprite_t *sprite, behavior_gen_t *behavior_gen, int length);

/*
 * Utility function used to generate direction lists for the movement behaviors
 */
void sprite_create_behavior_dir_list(move_behavior_t *move_behavior, direction_t *directions, int length);