#pragma once

#include <types.h>

typedef enum { GREEN, WHITE, YELLOW } color_t;

typedef struct {
  color_t color;
  uint8_t x;
  uint8_t y;
} coordinates_t;

typedef struct {

} scene_t;