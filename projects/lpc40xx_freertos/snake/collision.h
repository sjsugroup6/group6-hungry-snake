#pragma once

#include "game.h"
#include "sprite.h"

typedef enum {
  NO_ACTION_OLD_DOMINANT,
  NO_ACTION_NEW_DOMINANT,
  OLD_DIES,
  NEW_DIES,
  BOTH_DIES,
} collision_status_t;

/**
 * Handle sprite collision
 */
collision_status_t sprite_collision(game_grid_t *gg, int x, int y, sprite_t *old_sprite, sprite_t *new_sprite);