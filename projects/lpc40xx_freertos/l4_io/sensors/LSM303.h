#pragma once

#include "gpio_lab.h"
#include "i2c.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// this is the header file for LSM303, which is a compass + accelerometer module

#define ACCEL_ADDRESS 0x32
#define ACCEL_CTRL_REG1_A 0x20
#define ACCEL_CTRL_REG4_A 0x23
#define ACCEL_OUT_X_L_A 0x28
#define ACCEL_OUT_X_H_A 0x29
#define ACCEL_OUT_Y_L_A 0x2A
#define ACCEL_OUT_Y_H_A 0x2B
#define ACCEL_OUT_Z_L_A 0x2C
#define ACCEL_OUT_Z_H_A 0x2D

typedef struct {
  float x;
  float y;
  float z;
} LSM303_accel_type;

void lsm303__init(void);
LSM303_accel_type lsm303__update_accel_data(void);
void lsm303__check_if_shaking(float *number_of_shaking_detected);