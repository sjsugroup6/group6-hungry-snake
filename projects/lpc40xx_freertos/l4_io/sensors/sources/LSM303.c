#include "lsm303.h"

static LSM303_accel_type accel_sensor;
static float lsm303__calculate_acceleration_from_raw(int16_t raw_value);
static void lsm303__get_accel_data(void);

void lsm303__init(void) {
  // write to control register to turn on sensor @ 100Hz, normal mode -> CTRL_REG1_A should be 0b01010111
  i2c__write_single(I2C__2, ACCEL_ADDRESS, ACCEL_CTRL_REG1_A, 0x57);

  // set range to 4G: bit 4 should be set in reg 4
  i2c__write_single(I2C__2, ACCEL_ADDRESS, ACCEL_CTRL_REG4_A, 1 << 4);
}

static void lsm303__get_accel_data(void) {
  // for each axis, read 2 bytes from register address starting from 0x28.
  // bytes are expressed in 2's complement format
  // enable automatic incrementing address by asserting most sig. bit of register address
  uint8_t bytes_read[6] = {0};
  i2c__read_slave_data(I2C__2, ACCEL_ADDRESS, (1 << 7 | ACCEL_OUT_X_L_A), bytes_read, 6);
  // convert data to proper format
  int16_t accel_raw_data_X = (int16_t)(bytes_read[0] | (bytes_read[1] << 8));
  int16_t accel_raw_data_Y = (int16_t)(bytes_read[2] | (bytes_read[3] << 8));
  int16_t accel_raw_data_Z = (int16_t)(bytes_read[4] | (bytes_read[5] << 8));

  // fprintf(stderr, "x: %d %d, y: %d %d, z: %d %d\n", bytes_read[0], bytes_read[1], bytes_read[2], bytes_read[3],
  // bytes_read[4], bytes_read[5]);
  // update accelerometer value in struct
  accel_sensor.x = lsm303__calculate_acceleration_from_raw(accel_raw_data_X);
  accel_sensor.y = lsm303__calculate_acceleration_from_raw(accel_raw_data_Y);
  accel_sensor.z = lsm303__calculate_acceleration_from_raw(accel_raw_data_Z);
}

void lsm303__check_if_shaking(float *number_of_shaking_detected) {

  // figure out if someone is shaking the controller or not
  // when stationary, x and y accel are near zero, while z is ~9.8 due to gravity
  // criteria: take z reading. if absolute value of (reading - 9.5) > 20, then movement is detected
  lsm303__get_accel_data();
  if (abs(accel_sensor.z - 9.8) > 20) {
    // shaking detected once
    (*number_of_shaking_detected) += 1;
  }
}

static float lsm303__calculate_acceleration_from_raw(int16_t raw_value) {
  const float lsb = 0.00782; // for 4G, normal mode
  const uint8_t shift = 6;   // for normal mode
  const float sensor_gravity_standard = 9.80665;

  return (float)(raw_value >> shift) * lsb * sensor_gravity_standard;
}