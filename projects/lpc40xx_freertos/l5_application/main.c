#include <stdio.h>

#include "FreeRTOS.h"
#include "LED_matrix.h"
#include "adc.h"
#include "controller.h"
#include "draw.h"
#include "map.h"
#include "music.h"
#include "task.h"

#include "board_io.h"
#include "common_macros.h"
#include "i2c.h"
#include "periodic_scheduler.h"
#include "semphr.h"
#include "sj2_cli.h"

// 'static' to make these functions 'private' to this file
static void create_uart_task(void);
static void uart_task(void *params);
static void update_task(void *param);
static void check_shaking_task(void *param);
static void draw_task(void *param);
static void led_refresh_task(void *param);
static void print_out_controller_values(controller_type *controller);
static void activate_shaking();
static void dead_sound_callback();
static void bgm_sound_callback();
static void faster_bgm_sound_callback();
static void faster_bgm_sound_callback();
static void play_map(game_grid_t *map);

// Mutex to protect controller resource
static xSemaphoreHandle controller_mutex;

// Binary semaphore to start detecting shaking
static xSemaphoreHandle shaking_semaphore;

// queue to start/stop music
static QueueHandle_t music_queue;

// The current instance of the game
static game_grid_t *map;

static void dead_sound_callback() {
  int value = 2;
  xQueueSend(music_queue, &value, 0);
}

static void bgm_sound_callback() {
  int value = 1;
  xQueueSend(music_queue, &value, 0);
}

static void faster_bgm_sound_callback() {
  int value = 4;
  xQueueSend(music_queue, &value, 0);
}

static void eat_sound_callback() {
  int value = 3;
  xQueueSend(music_queue, &value, 0);
}

static void led_refresh_task(void *param) {
  led_matrix__init_pins();
  while (1) {
    led_matrix__refreshDisplay();
    vTaskDelay(10);
  }
}

static void activate_shaking() { xSemaphoreGive(shaking_semaphore); }

// This is just a prototype to show a snake and obstacle on the LED
static void draw_task(void *param) {
  controller_type controller;
  int difficulty_selected = 0; // 0 - easy difficulty, 1 - medium, 2 - hard
  controller_selection start_screen_selection = difficulty;
  bool game_in_progress = false;
  map_create map_init_func;

  /*
    delay__ms(1000);
    // bgm_sound_callback();
    while (1) {
      // play_bgm_music();
      bgm_sound_callback();
      delay__ms(5000);
      faster_bgm_sound_callback();
      delay__ms(1000);
      eat_sound_callback();
      delay__ms(1000);
      dead_sound_callback();

      // play_bgm_music();
      // play_bgm_music();
      // delay__ms(1000);
      // play_eat_sound();
      // delay__ms(1000);
      // play_faster_music();
      // delay__ms(1000);
      // play_dead_sound();
      // delay__ms(1000);
    }
  */
  vTaskDelay(1000);
  bgm_sound_callback();
  // stages
  while (1) {
    // menu selection refresh
    if (!game_in_progress) {
      draw_start_screen(difficulty_selected, start_screen_selection);
    }

    // check which label is highlighted
    if (!game_in_progress && xSemaphoreTake(controller_mutex, 0)) {
      controller = controller__get_value();
      xSemaphoreGive(controller_mutex);
      if (controller.current_direction == DOWN) {
        start_screen_selection = start; // "start" is highlighted
      } else if (controller.current_direction == UP) {
        start_screen_selection = difficulty; // "difficulty" is highlighted
      }
    }

    // if player selected "difficulty", swap between difficulty
    if (!game_in_progress && start_screen_selection == difficulty) {
      if (xSemaphoreTake(controller_mutex, 0)) {
        controller = controller__get_value();
        xSemaphoreGive(controller_mutex);

        if (controller.current_direction == LEFT) {
          difficulty_selected--;
          if (difficulty_selected < 0)
            difficulty_selected = 0;
          delay__ms(200); // need this, or else it is too fast to switch between difficulties
        } else if (controller.current_direction == RIGHT) {
          difficulty_selected++;
          if (difficulty_selected > 3)
            difficulty_selected = 3;
          delay__ms(200);
        }
      }
    }

    // player needs to have "start" selected and press confirm to start game
    if (!game_in_progress && start_screen_selection == start) {
      if (xSemaphoreTake(controller_mutex, 0)) {
        controller = controller__get_value();
        xSemaphoreGive(controller_mutex);
        if (controller.is_yes_button_pressed == true) {
          // exit start screen and begin the snake game
          game_in_progress = true;
          switch (difficulty_selected) {
          case 0:
            map_init_func = init_easy_map;
            break;
          case 1:
            map_init_func = init_medium_map;
            break;
          case 2:
            map_init_func = init_hard_map;
            break;
          case 3:
            map_init_func = init_frogger_map; // TODO: frogger if we have time
            break;
          default:
            fprintf(stderr, "Unhandled difficulty level %d\n", difficulty_selected);
            map_init_func = init_easy_map;
          }
          map = map_init_func(bgm_sound_callback, faster_bgm_sound_callback, eat_sound_callback, dead_sound_callback,
                              activate_shaking);
          play_map(map);
          draw_end_screen(map);
          free_map(map);
        }
      }
    }

    // stage: end screen
    if (game_in_progress) {
      if (xSemaphoreTake(controller_mutex, 0)) {
        controller = controller__get_value();
        xSemaphoreGive(controller_mutex);
        if (controller.is_yes_button_pressed || controller.is_no_button_pressed) {
          // stage should switch to the start screen now
          bgm_sound_callback();
          game_in_progress = false;
          vTaskDelay(200); // Prevent detection of switch bounce which will immediately start a new map
          clear_screen();
        }
      }
    }

    vTaskDelay(20);
  }
}

static void play_map(game_grid_t *map) {
  // Let's make sure the first sprite is the player snake
  sprite_t *snake = map->sprite_list->sprite;
  direction_t new_dir;
  controller_type controller;

  while (1) {
    if (xSemaphoreTake(controller_mutex, 0)) {
      controller = controller__get_value();
      xSemaphoreGive(controller_mutex);
    }
    new_dir = controller.current_direction;
    if (new_dir != NONE) {
      sprite_direction_set(snake, new_dir);
    }

    if (game_step(map)) {
      return;
    }

    clear_screen();
    draw_banner(map);
    draw_sprites(map->sprite_list);
    vTaskDelay(20);
  }
}

static void update_task(void *param) {
  // initialize controller
  controller__init();
  while (1) {
    if (xSemaphoreTake(controller_mutex, 50)) {
      controller__update();
      xSemaphoreGive(controller_mutex);
    }
    vTaskDelay(100);
  }
}

// task starts by getting a binary semaphore.
// Displays the progress bar that increases while player shakes the controller
static void check_shaking_task(void *param) {
  lsm303__init();
  const int number_of_shaking_threshold = 128;
  // initialize temp struct to store controller values
  float initial_shaking_value = 64;
  float number_of_shaking_detected = initial_shaking_value;
  controller__progress_number_setter(number_of_shaking_detected);
  while (1) {
    if (xSemaphoreTake(shaking_semaphore, 100)) {
      while (number_of_shaking_detected > 0 && number_of_shaking_detected < number_of_shaking_threshold) {
        set_current_banner(progress_bar);
        lsm303__check_if_shaking(&number_of_shaking_detected);
        number_of_shaking_detected -= 0.4;
        controller__progress_number_setter(number_of_shaking_detected);
        delay__ms(10);
      }

      if (round(number_of_shaking_detected)) {
        win_battle(map);
      } else {
        set_gameover(map);
      }

      // progress finished!
      number_of_shaking_detected = initial_shaking_value;
      controller__progress_number_setter(number_of_shaking_detected);
      // something else needs to happen to go back to regular play session
      set_current_banner(score); // go back to display score
    }
  }
}

// To play music, send a music_list enum to the music_queue
static void music_task(void *param) {
  // initialize music gpios
  music_list received_music_value;
  bool is_faster_music_playing = false;
  init_music_pins();
  while (1) {
    if (xQueueReceive(music_queue, &received_music_value, 100)) {
      fprintf(stderr, "Received queue value: %d\n", received_music_value);
      switch (received_music_value) {
      case NA:
        // Do nothing here
        break;
      case BGM:
        play_bgm_music();
        is_faster_music_playing = false;
        break;
      case DEAD:
        play_dead_sound();
        break;
      case EAT:
        play_eat_sound();
        delay__ms(450);
        play_bgm_music();
        break;
      case FASTER:
        play_faster_music();
        is_faster_music_playing = true;
        break;
      default:
        fprintf(stderr, "play music task error \n");
      }
    }
  }
}

int main(void) {
  create_uart_task();
  init_music_pins();
  controller_mutex = xSemaphoreCreateMutex();
  shaking_semaphore = xSemaphoreCreateBinary();
  music_queue = xQueueCreate(3, sizeof(music_list));

  xTaskCreate(led_refresh_task, "led refresh task", 4096 / sizeof(void *), NULL, PRIORITY_HIGH, NULL);
  xTaskCreate(draw_task, "draw task", 4096 / sizeof(void *), NULL, PRIORITY_LOW, NULL);
  xTaskCreate(update_task, "controller update task", 4096 / sizeof(void *), NULL, PRIORITY_LOW, NULL);
  xTaskCreate(check_shaking_task, "check if shaking happened", 4096 / sizeof(void *), NULL, PRIORITY_LOW, NULL);
  xTaskCreate(music_task, "task to play/stop music", 4096 / sizeof(void *), NULL, PRIORITY_LOW, NULL);
  vTaskStartScheduler();
  return 0;
}

static void create_uart_task(void) {
  // It is advised to either run the uart_task, or the SJ2 command-line (CLI), but not both
  // Change '#if (0)' to '#if (1)' and vice versa to try it out
#if (0)
  // printf() takes more stack space, size this tasks' stack higher
  xTaskCreate(uart_task, "uart", (512U * 8) / sizeof(void *), NULL, PRIORITY_LOW, NULL);
#else
  sj2_cli__init();
  UNUSED(uart_task); // uart_task is un-used in if we are doing cli init()
#endif
}

// This sends periodic messages over printf() which uses system_calls.c to send them to UART0
static void uart_task(void *params) {
  TickType_t previous_tick = 0;
  TickType_t ticks = 0;

  while (true) {
    // This loop will repeat at precise task delay, even if the logic below takes variable amount of ticks
    vTaskDelayUntil(&previous_tick, 2000);

    /* Calls to fprintf(stderr, ...) uses polled UART driver, so this entire output will be fully
     * sent out before this function returns. See system_calls.c for actual implementation.
     *
     * Use this style print for:
     *  - Interrupts because you cannot use printf() inside an ISR
     *    This is because regular printf() leads down to xQueueSend() that might block
     *    but you cannot block inside an ISR hence the system might crash
     *  - During debugging in case system crashes before all output of printf() is sent
     */
    ticks = xTaskGetTickCount();
    fprintf(stderr, "%u: This is a polled version of printf used for debugging ... finished in", (unsigned)ticks);
    fprintf(stderr, " %lu ticks\n", (xTaskGetTickCount() - ticks));

    /* This deposits data to an outgoing queue and doesn't block the CPU
     * Data will be sent later, but this function would return earlier
     */
    ticks = xTaskGetTickCount();
    printf("This is a more efficient printf ... finished in");
    printf(" %lu ticks\n\n", (xTaskGetTickCount() - ticks));
  }
}

static void print_out_controller_values(controller_type *controller) {
  switch (controller->current_direction) {
  case NONE:
    fprintf(stderr, "None ");
    break;
  case UP:
    fprintf(stderr, "Up ");
    break;
  case DOWN:
    fprintf(stderr, "Down ");
    break;
  case LEFT:
    fprintf(stderr, "Left ");
    break;
  case RIGHT:
    fprintf(stderr, "Right ");
    break;
  }

  fprintf(stderr, "yes button: %d, no button %d\n", controller->is_yes_button_pressed,
          controller->is_no_button_pressed);
}
